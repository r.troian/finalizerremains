﻿using RabbitMQ.Client;
using ServiceStack.Text;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Consimple.Workers
{
    public static class RMQ
    {
        private static IConnection _connection;

        public static void SetConnection()
        {
            if (_connection == null)
                _connection = Config.RmqConnection.GetConnection();
        }

        public static void Publish<T>(string queue, string exchange, List<T> data)
        {
            SetConnection();

            while (!_connection.IsOpen)
            {
                Config.RmqConnection.BuildConnection();
                Config.logger.Information("Try reopen connection to Rabbitmq");
            }

            if (data == null || data.Count() == 0)
                return;

            using (var channel = _connection.CreateModel())
            {
                channel.QueueDeclare(queue: queue,
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                foreach (var item in data)
                {
                    var body = Encoding.UTF8.GetBytes(JsonSerializer.SerializeToString(item));
                    channel.BasicPublish(exchange: exchange, routingKey: "", basicProperties: null, body: body);
                }
            }
        }

        public static void Publish<T>(string queue, string exchange, T data)
        {
            SetConnection();

            while (!_connection.IsOpen)
            {
                Config.RmqConnection.BuildConnection();
                Config.logger.Information("Try reopen connection to Rabbitmq");
            }

            if (data == null)
                return;

            using (var channel = _connection.CreateModel())
            {
                channel.QueueDeclare(queue: queue,
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);


                var body = Encoding.UTF8.GetBytes(JsonSerializer.SerializeToString(data));
                channel.BasicPublish(exchange: exchange, routingKey: "", basicProperties: null, body: body);
            }
        }
        
        public static void Publish(string queue, string exchange, byte[] data)
        {
            SetConnection();

            while (!_connection.IsOpen)
            {
                Config.RmqConnection.BuildConnection();
                Config.logger.Information("Try reopen connection to Rabbitmq");
            }

            if (data == null || data.Count() == 0)
                return;

            using (var channel = _connection.CreateModel())
            {
                channel.QueueDeclare(queue: queue,
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                    channel.BasicPublish(exchange: exchange, routingKey: "", basicProperties: null, body: data);
                
            }
        }

    }
}
