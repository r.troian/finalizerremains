﻿using System.Threading;

using Serilogger;
using Serilogger.Events;
using Serilogger.Core;

namespace Consimple.Workers
{
    public static class loggerger
    {
        private static object obj = new object();
        public static Serilogger.Core.loggerger InitConfiguration(string directory)
        {
            return new loggergerConfiguration()
                .Enrich.With(new ThreadIdEnricher())
                .MinimumLevel.Debug()
                .WriteTo.ColoredConsole(restrictedToMinimumLevel: loggerEventLevel.Information)
                .WriteTo.File($"{directory}\\loggers\\system-logger-.txt", rollingInterval: RollingInterval.Day,
                    outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}")
                .Createloggerger();
        }

        public static void loggerError(this Serilogger.Core.loggerger loggerger, string queue, string message)
        {
            lock(obj)
            {
                loggerger.Error($"=> SyncBpmProcessing_{ queue }: {message}");    
            }
        }

        public static void loggerInfo(this Serilogger.Core.loggerger loggerger, string queue, string message)
        {
            lock (obj)
            {
                loggerger.Information($"=> SyncBpmProcessing_{ queue }: {message}");
            }
        }

        public static Serilogger.Core.loggerger InitConfiguration(string fileName, string directory)
        {
            return new loggergerConfiguration()
                .Enrich.With(new ThreadIdEnricher())
                .MinimumLevel.Debug()
                .WriteTo.ColoredConsole(restrictedToMinimumLevel: loggerEventLevel.Information)
                .WriteTo.File($"{directory}\\loggers\\{fileName}-logger-.txt", rollingInterval: RollingInterval.Day,
                    outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}")
                .Createloggerger();
        }
    }

    public class ThreadIdEnricher : IloggerEventEnricher
    {
        public void Enrich(loggerEvent loggerEvent, IloggerEventPropertyFactory propertyFactory)
        {
            loggerEvent.AddPropertyIfAbsent(propertyFactory.CreateProperty(
                    "ThreadId", Thread.CurrentThread.ManagedThreadId));
        }
    }
}
