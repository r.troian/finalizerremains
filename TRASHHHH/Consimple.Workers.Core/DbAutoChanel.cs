﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using Dapper;
using RabbitMQ.Client;
using System.Threading;
using Consimple.Workers.Db;

namespace Consimple.Workers
{
    public class DbAutoChannel : ChannelBase, IChannel
    {

        #region properties   
        private int _totalRecordsCount = 0;
        private int _addedRecordsCount = 0;
        private int _errRecordsCount = 0;
        private int _pakCount = 1;
        private int _retryCount = 0;
        private DateTime _lastPakSynchronized;
        private Stopwatch _sw;
        #endregion

        public override dynamic Map(ModelBuilder model, dynamic row)
        {
            var curRow = new Dictionary<string, object>();

            foreach (var column in row)
            {
                try
                {
                    var rules = mapper.GetMapRules(model, column.Key);

                    if (rules.Count != 0)
                    {
                        mapper.GetColumnValue(rules, curRow, column, model);
                    }
                    else if (model.targetColumns.Contains(column.Key))
                    {
                        mapper.GetColumnValue(curRow, column, model);
                    }
                }
                catch (Exception ex)
                {
                    logger.LogError(model.config.Queue, $"{ex.Message};");
                }
            }

            return curRow;

        }


        public DbAutoChannel(SyncList sync)
                            :base(sync)
        {
            _sw = new Stopwatch();
        }
     
        override public void Listen(IConnection connection = null)
        {
            var srcDataList = new List<dynamic>();
            BuildModels();
            StartListen();

            while (true)
            {
                try
                {
                    srcDataList = GetList();

                    if (srcDataList.Count > 0)
                    {
                        var trgDataList = new List<dynamic>();

                        foreach (var row in srcDataList)
                            trgDataList.Add(Map(modelBuilder, row));

                        Write(trgDataList);

                        ApplyWritedCounters();
                    }

                    UpdateLastPakSynchronized();
                }
                catch(Exception ex)
                {
                    logger.LogError(sync.ModelNameTarget, $"{ex.Message} in {ex.Source}; ");

                  //  ApplyWritedCounters();
                }

                Thread.Sleep(sync.SyncTimeout);
            }
        }

        protected void StartListen()
        {
            logger.LogInfo(sync.ModelNameTarget, "Start consuming");

            UpdateLastPakSynchronized();
            UpdateTotalRecordsCount();
            UpdatePakCount();
            _sw.Start();
        }

        #region counter iteration
        protected void ApplyWritedCounters()
        {
            if (_addedRecordsCount != 0 && _totalRecordsCount != 0)
                logger.LogInfo(sync.ModelNameTarget, $"{_addedRecordsCount} err[{_errRecordsCount}] of {_totalRecordsCount} data synchronized for {_sw.Elapsed}");

            if (_addedRecordsCount == _totalRecordsCount)
            {
                ResetCounters();
                ApplyEquels();
            }
            if ((_addedRecordsCount > _totalRecordsCount) || (_retryCount == _pakCount))
            {
                ResetCounters();
                TotalInserted();
            }
            else
                _retryCount++;
        }

        protected void ApplyEquels()
        {
            sync.SyncDate = _lastPakSynchronized.AddSeconds(Config.ToSelectPrevDataSeconds);
            UpdateLastPakSynchronized();
            UpdateTotalRecordsCount();
            UpdatePakCount();

            _retryCount = 0;
            _sw.Restart();
        }

        protected void TotalInserted()
        {
            UpdateTotalRecordsCount();
            UpdatePakCount();
            _retryCount = 0;
        }

        protected void UpdatePakCount()
        {
            _pakCount = _totalRecordsCount > Config.MaxSelectCount ?
                (int)Math.Ceiling((decimal)_totalRecordsCount / (decimal)Config.MaxSelectCount) : 1;
        }

        protected void UpdateTotalRecordsCount()
        {
            var sql = modelBuilder.SourceBuilder.GetCountByDateQuery(sync.ModelNameSource);
            using (var db = DbConnector.GetConnection(source, ConnectionStringType.Select))
            {
                var d1 = sync.SyncDate.ToString("yyyy-MM-dd HH:mm:ss.fff");
                var d2 = _lastPakSynchronized.ToString("yyyy-MM-dd HH:mm:ss.fff");

                _totalRecordsCount = db.ExecuteScalar<int>(sql,
                    new { ModifiedOn = sync.SyncDate, LastPakSynchronized = _lastPakSynchronized } , null, 0);

                //sql = $@"SELECT COUNT(*) FROM Contact WHERE ModifiedOn >= '{sync.SyncDate.ToString("yyyy-MM-dd HH:mm:ss.fff")}' 
                //       AND   ModifiedOn <= '{_lastPakSynchronized.ToString("yyyy-MM-dd HH:mm:ss.fff")}' ;";

                //_totalRecordsCount = db.ExecuteScalar<int>(sql);//,
                //new { ModifiedOn = sync.SyncDate.ToString("yyyy-MM-dd HH:mm:ss.fff"), LastPakSynchronized = _lastPakSynchronized.ToString("yyyy-MM-dd HH:mm:ss.fff") });

            
            }
        }

        protected void UpdateLastPakSynchronized()
        {
            _lastPakSynchronized = DateTime.UtcNow;
        }

        protected void ResetCounters()
        {
            _addedRecordsCount = 0;
            _totalRecordsCount = 0;
        }
        #endregion

        protected List<dynamic> GetList()
        {
            var srcDataList = new List<dynamic>();
            string sql = getListQuery + modelBuilder.SourceBuilder.Offset(_addedRecordsCount);

            try
            {
                using (var db = DbConnector.GetConnection(source, ConnectionStringType.Select))
                {
                    srcDataList = db.Query(sql, new { ModifiedOn = sync.SyncDate, LastPakSynchronized = _lastPakSynchronized }, null, true, 0).ToList();
                }
            }
            catch (DbException ex)
            {
                logger.LogError(sync.ModelNameTarget, $"{ex.Message} in {ex.Source}; InnerException: {ex.InnerException}");

                return srcDataList;
            }

            return srcDataList;
            
        }

        protected void Write(List<dynamic> trgDataList)
        {
            using (var db = DbConnector.GetConnection(target, ConnectionStringType.Insert))
            {
                foreach (var item in trgDataList)
                {
                    try
                    {
                        UpdateInsert(db, item, modelBuilder);
                        _addedRecordsCount++;
                    }
                    catch (Exception ex)
                    {
                        _addedRecordsCount++;
                        _errRecordsCount++;
                      logger.LogError(sync.ModelNameTarget, $"{ex.Message} in {ex.Source}; ");                   
                    }
                }
            }
        }
    }
}
