﻿using Dapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using RabbitMQ.Client;
using RabbitMQ.Client.Events;

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;

using Consimple.Workers.Db;
using MySql.Data.MySqlClient;

namespace Consimple.Workers
{
   public class RMQChannel: ChannelBase, IChannel
   {
        #region fields   
        private dynamic _data;
        private dynamic _obj;
        private Dictionary<Guid, int>  _entityInsertionCount;
        private ChannelConfig _config;

        private IConnection _connection;
        private IModel _channel;
        #endregion

        public override dynamic Map(ModelBuilder model, dynamic row)
        {
            var curRow = new Dictionary<string, object>();
            var targetColumns = new List<string>();

            foreach (var column in row)
            {
                try
                {
                    var rules = mapper.GetMapRules(model, column.Key);

                    if (rules.Count != 0)
                    {
                        mapper.GetColumnValue(rules, curRow, column, model, targetColumns);
                    }
                    else if (model.sourceColumns.Contains(column.Key))
                    {
                        mapper.GetColumnValue(curRow, column, model);
                        targetColumns.Add(column.Key);
                    }
                }
                catch (Exception ex)
                {
                    //loggerError(model.config.Queue, $"{ex.Message};");
                }
            }

            model.targetColumns = targetColumns;
  
            return curRow;
        }

        public RMQChannel(SyncList sync) : base(sync)
        {
            SetConfig(new ChannelConfig
            {
                Queue = sync.Queue,
                Durable = true,
                Exclusive = false,
                AutoDelete = false,
                Arguments = null,
                Exchange = sync.Exchange,
                Target = (DbType)sync.SystemTarget,
                Source = (DbType)sync.SystemSource,
                SyncTimeout = sync.SyncTimeout
            });
        }

        public void SetConfig(ChannelConfig config)
        {
            _config = config;
            Name = config.Queue;
        }

        public void DeclareRmq(IConnection connection = null)
        {
            if (_connection == null || !_connection.IsOpen)
            {
                _connection = connection;

                if (_channel == null)
                {
                    _channel = _connection.CreateModel();
                    _channel.QueueDeclare(_config);
                    _channel.ExchangeDeclare(_config.Exchange, "fanout", true);
                    _channel.QueueBind
                    (
                        queue: _config.Queue,
                        exchange: _config.Exchange,
                        routingKey: ""
                    );
                }
            }
        }

        override  public void Listen(IConnection connection)
        {
            DeclareRmq(connection);
            var consumer = new EventingBasicConsumer(_channel);           
            loggerger.loggerInfo(_config.Queue, "Start consuming");
            BuildModels();
            _entityInsertionCount = new Dictionary<Guid, int>();
            _channel.BasicConsume(_config.Queue, false, consumer);
            consumer.Received += (model, ea) =>
            {
                loggerger.loggerInfo(_config.Queue, "Got message");
                _obj = JsonConvert.DeserializeObject<Dictionary<string, object>>(Encoding.UTF8.GetString(ea.Body));

                if (modelBuilder.source == DbType.MSSQL)
                {
                    //For Transaction 
                    var values = JArray.FromObject(_obj["Entitites"]);
                    foreach (var value in values)
                    {
                        _data = Map(modelBuilder, value.ToObject<Dictionary<string, object>>());
                        Write(ea, modelBuilder, _data);
                    }
                    //^^
                }
                else
                {
                    //For Purchase
                    _data = Map(modelBuilder, _obj);
                    Write(ea, modelBuilder, _data);
                    //^^
                }

                _channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                Thread.Sleep(_config.SleepTime);
            };
        }

        public void Write(BasicDeliverEventArgs ea, ModelBuilder model, dynamic _data)
        {
            if (_data == null)
                return;

            var entityId = Guid.Empty;
            try
            {
                using (var db = DbConnector.GetConnection(target, ConnectionStringType.Insert))
                {
                    entityId = _data["Id"] is Guid ? _data["Id"] : new Guid(_data["Id"]);              

                    UpdateInsert(db, _data, model);
                    Config.UpdateSyncDate(_config.Queue);

                    if (model.SubModels.Count > 0)
                        GetFKeys(db, model, _obj);

                    CalculateOrderBonusesAmount(db, entityId, model);

                    db.Close();
                    RemoveData(_data);         
                    _entityInsertionCount.Remove(entityId);
                }
            }
            catch (SqlException ex)
            {
                SqlExceptionHandler(ex, entityId,  ea, model);
            }
            catch (MySqlException ex)
            {
                SqlExceptionHandler(ex, entityId, ea, model);
            }
            catch (Exception ex)
            {
               // if(! ex.Message.Contains("Guid should contain 32 digits with 4 dashes"))
                    RMQ.Publish(_config.Queue, _config.Exchange, ea.Body);

                loggerger.loggerError(_config.Queue, $"{ex.Message}; {ex.Source}");                 
            }     
        }

        public void GetFKeys(IDbConnection db, ModelBuilder modelBuilder, dynamic _obj)
        {
            foreach (var item in _obj)
            {
                if (sync.FK.Exists(fk => fk.Queue == item.Key))
                {
                    var s = sync.FK.Where(fk => fk.Queue == item.Key).First();
                    var model = modelBuilder.SubModels.Where(mb => mb.NameTarget == s.ModelNameTarget).First();
                    WriteFKeys(db, model, item);
                }
            }
        }

        public void WriteFKeys(IDbConnection db, ModelBuilder model, dynamic item)
        {
            for (int i = 0; i < item.Value.Count; i++)
            {
                var values = JObject.FromObject(item.Value[i]).ToObject<Dictionary<string, object>>();
                var data = Map(model, values);

                if (data.Count != 0)
                    UpdateInsert(db, data, model);
            }

            if (item.Value.Count == null && item.Value != null)
            {
                var values = JObject.FromObject(item.Value).ToObject<Dictionary<string, object>>();
                var data = Map(model, values);

                if (data.Count != 0)
                    UpdateInsert(db, data, model);
            }
         }

        public void CalculateOrderBonusesAmount(IDbConnection db, Guid entityId, ModelBuilder model)
        {
            if (model.config.IsNeedUpdate && !string.IsNullOrEmpty(Config.procedureName))
            {
                if (db.State == ConnectionState.Closed )
                    db.Open();

                db.Execute(Config.procedureName, new { order_id = entityId }, commandType: CommandType.StoredProcedure);     
            }
        }

        private void SqlExceptionHandler(Exception ex, Guid purhaseId, BasicDeliverEventArgs ea, ModelBuilder model)
        {
            loggerger.loggerError(_config.Queue, $"{ex.Message}; {ex.Source}");

           
                if (_entityInsertionCount.ContainsKey(purhaseId))
                    _entityInsertionCount[purhaseId]++;
                else
                    _entityInsertionCount.Add(purhaseId, 0);
            

            if (_entityInsertionCount[purhaseId] < model.config.MaxInsertAttempts)
            {
                RMQ.Publish(_config.Queue, _config.Exchange, ea.Body);
            }
            else
            {
                _entityInsertionCount.Remove(purhaseId);
                InsertIntoLostOrder(ea, ex.Message);
            }
        }

        private void InsertIntoLostOrder(BasicDeliverEventArgs ea, string errorMessage)
        {
            try
            {
                using (var db = DbConnector.GetConnection(target, ConnectionStringType.Insert))
                {
                    var s = (Encoding.UTF8.GetString(ea.Body));
                    string sql = $"INSERT INTO {Config.tableForLostOrders} ([ErrorMessage], [OrderData]) VALUES (@ErrorMessage, @OrderData)";
                    Dapper.SqlMapper.Execute(db, sql: sql, param: new { ErrorMessage = errorMessage, OrderData = s });
                }
            }
            catch (Exception ex)
            {
                loggerger.loggerError(_config.Queue, $"{ex.Message}; {ex.Source}");
            }
        }

        private void RemoveData(dynamic data)
        {
            _data = null;
        }
   }
}
