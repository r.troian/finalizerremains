﻿using System.Data;
using Dapper.Ext;

namespace Consimple.Workers.Db
{
    public class OrmDbConnector
    {
        private static OrmConnectionFactory _factory;

        public OrmDbConnector()
        {
            if (_factory == null)
                InitFactory();
        }

        public static void InitFactory()
        {
            _factory = new OrmConnectionFactory(Config.BpmConnection, SqlDialect.Provider);
            _factory.RegisterConnection("mysql", new OrmConnectionFactory(Config.PerconaConnection, MySqlDialect.Provider));
        }

        public IDbConnection GetConnection(DbType target)
        {
            if (target == DbType.MYSQL)
                return GetPerconaConnection();
            else
                return GetBpmConnection();
        }

        private IDbConnection GetPerconaConnection()
        {
            return _factory.OpenDbConnection("mysql");
        }

        private IDbConnection GetBpmConnection()
        {
            return _factory.OpenDbConnection();
        }
    }
}
