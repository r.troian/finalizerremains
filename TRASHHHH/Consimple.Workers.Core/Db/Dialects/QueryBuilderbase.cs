﻿using Consimple.Workers.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Consimple.Workers.Db
{
    public class QueryBuilderBase : IDialect
    {
        public virtual string GetQuotedColumn(string columnName)
        {
            return columnName;
        }

        public virtual string GetQuotedTable(string tableName)
        {
            return tableName;
        }

        public virtual string SelectTopAllFrom(string tableName)
        {
            return $"SELECT * FROM {GetQuotedTable(tableName)} LIMIT 1";
        }

        public virtual string GetListFromSource(string tableName, List<string> cols)
        {
            var modifiedOn = GetQuotedColumn("ModifiedOn");
            var columnsDefs = GetColumnDefs(cols);
            return $@"
SELECT {columnsDefs.ToString()}
FROM {GetQuotedTable(tableName)}
WHERE {modifiedOn} >= @ModifiedOn
AND {modifiedOn} <= @LastPakSynchronized
ORDER BY {modifiedOn} ASC ";
        }

        public virtual string Offset(int addedRecordsCount)
        {
            return $"LIMIT {Config.MaxSelectCount} OFFSET {addedRecordsCount};";
        }

        protected virtual StringBuilder GetColumnDefs(List<string> cols)
        {
            var sb = new StringBuilder();
            var count = cols.Count - 1;
            for (var i = 0; i < count; i++)
            {
                sb.Append(GetQuotedColumn(cols[i]));
                sb.Append(", ");
            }
            sb.Append(GetQuotedColumn(cols[count]));
            return sb;
        }

        public string GetUpdateStatement(string tableName, List<string> columns, string primary = "Id", string condition = null)
        {
            var query = new StringBuilder();
            query.Append($"UPDATE {GetQuotedTable(tableName)} SET\n");
            var count = columns.Count - 1;
            for (var i = 0; i < count; i++)
            {
                if (columns[i] != primary)
                {
                    condition = condition ?? GetCondition(columns[i]);
                    query.Append($"\t{GetQuotedColumn(columns[i])} = @{columns[i]}");
                    query.Append(",\n");
                }
            }
            query.Append($"\t{GetQuotedColumn(columns[count])} = @{columns[count]}\n");
            query.Append($"WHERE {GetQuotedColumn(primary)} = @{primary}{condition};");
            return query.ToString();
        }

        public string GetInsertStatement(string tableName, List<string> columns)
        {
          var query = new StringBuilder();
            query.Append($"INSERT INTO {GetQuotedTable(tableName)}\n");
            query.Append($"({GetColumnDefs(columns)})\n");
            query.Append($" VALUES \n(");
            var count = columns.Count - 1;
            for (var i = 0; i < count; i++)
            {
                query.Append($"@{columns[i]}, ");
            }
            query.Append($"@{columns[count]});");
            return query.ToString();
        }

        public string GetCountByDateQuery(string systemSource)
        {
            return $@"
            SELECT COUNT(*) 
            FROM {GetQuotedTable(systemSource)} 
            WHERE {GetQuotedColumn("ModifiedOn")}  >= @ModifiedOn
            AND {GetQuotedColumn("ModifiedOn")} <= @LastPakSynchronized";
        }

        protected virtual string GetCondition(string param, string field = "ModifiedOn")
        {
            if (param != field) return null;
            return $" AND {GetQuotedColumn(field)} < @{field}";
        }
    }
}
