﻿namespace Consimple.Workers.Db
{
    public class SqlBuilder : QueryBuilderBase
    {
        public override string GetQuotedColumn(string columnName)
        {
            return $"[{columnName}]";
        }

        public override string GetQuotedTable(string tableName)
        {
            return $"[{tableName}]";
        }        

        public override string SelectTopAllFrom(string tableName)
        {
            return $"SELECT TOP 1 * FROM {GetQuotedTable(tableName)}";
        }

        public override string Offset(int addedRecordsCount)
        {
            return $@"OFFSET {addedRecordsCount} ROWS
                        FETCH NEXT {Config.MaxSelectCount} ROWS ONLY;";
        }
    }
}
