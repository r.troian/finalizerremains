﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using Consimple.Workers.Db;

namespace Consimple.Workers
{
    public interface IMapper
    {
        ChannelType Type { get; set; }

        List<MapRules> GetMapRules(ModelBuilder model, string name);

        void GetColumnValue(Dictionary<string, object> curRow, dynamic column, ModelBuilder model, string ruleName = null);

        void GetColumnValue(List<MapRules> rules, Dictionary<string, object> curRow,
                                    dynamic column, ModelBuilder model, List<string> targetColumns = null);
    }

    public class MapperBase : IMapper
    {
        public ChannelType Type { get; set; }
        public DbType MapperSource { get; }
        public DbType MapperTarget { get; }
        public DbConverter Converter { get; }

        public MapperBase(ChannelType channelType, DbType targetDb)
        {
            Type = channelType;
            Converter = new DbConverter(targetDb);
        }

        public virtual void GetColumnValue(Dictionary<string, object> curRow, dynamic column, ModelBuilder model,
            string ruleName = null)
        {
            try
            {
                //if (column.Value == null)
                //{
                //    var tp = model.sourceColumnsBase
                //        .Where(scb => scb.Key == column.Key).First().Value;
                //    curRow.Add(ruleName ?? column.Key, GetDefault(tp));
                //    return;
                //}
                var temp = Converter.Db[model.sourceColumnsBase
                    .Where(scb => scb.Key == column.Key).First().Value](column.Value);
                curRow.Add(ruleName ?? column.Key, temp);
            }
            catch (KeyNotFoundException)
            {
                curRow.Add(ruleName ?? column.Key, column.Value);
            }
            catch (NullReferenceException)
            {
                var tp = model.sourceColumnsBase
                        .Where(scb => scb.Key == column.Key).First().Value;
                curRow.Add(ruleName ?? column.Key, GetDefault(tp));
                return;
            }
            catch (Exception ex)
            {
                Config.logger.Information($"ERROR :: {ex.Message}");
            }
        }

        public void GetColumnValue(List<MapRules> rules, Dictionary<string, object> curRow,
            dynamic column, ModelBuilder model, List<string> targetColumns = null)
        {
            foreach (var rule in rules)
            {
                switch (rule.RuleType)
                {
                    case Consimple.Workers.RuleType.Multiplication:
                        curRow.Add(rule.TargetColumnName, Convert.ToDecimal(column.Value) * Convert.ToDecimal(rule.Value));
                        break;
                    case Consimple.Workers.RuleType.DefaultValue:
                        curRow.Add(rule.TargetColumnName, new Guid(rule.Value));
                        break;
                    case Consimple.Workers.RuleType.Default:
                        curRow.Add(rule.TargetColumnName, rule.Value);
                        break;
                    case Consimple.Workers.RuleType.AddHours:
                        curRow.Add(rule.TargetColumnName,
                            (!string.IsNullOrEmpty(rule.Value) && column.Value is DateTime)
                            ? AddHoursToDateTime(column.Value, Convert.ToInt32(rule.Value)) : column.Value);
                        break;

                    //r.troian 07-04-2020 -->
                    case Consimple.Workers.RuleType.SimpleDate:
                        curRow.Add(rule.TargetColumnName, column.Value);
                        break;
                    //<---
                    case Consimple.Workers.RuleType.Ignore:
                        break;
                    case Consimple.Workers.RuleType.BAToGuid:
                        curRow.Add(rule.TargetColumnName, new Guid(Convert.FromBase64String(column.Value)));
                        break;
                    case Consimple.Workers.RuleType.GuidToBA:
                        curRow.Add(rule.TargetColumnName, column.Value.Replace("-", ""));
                        break;
                    case Consimple.Workers.RuleType.TimeZone:
                        curRow.Add(rule.TargetColumnName, GetTimeZoneDate(rule.Value, Convert.ToDateTime(column?.Value)));
                        break;
                    default: //Simple
                        GetColumnValue(curRow, column, model, rule.TargetColumnName);
                        break;
                }

                if (targetColumns != null && rule.RuleType != Consimple.Workers.RuleType.Ignore)
                    targetColumns.Add(rule.TargetColumnName);
            }
        }

        public List<MapRules> GetMapRules(ModelBuilder model, string name)
        {
            return model.config.MapRules.Where(s => s.SourceColumnName == name).ToList();
        }

        protected object GetDefault(Type type)
        {
            if (type.IsValueType)
            {
                return Activator.CreateInstance(type);
            }
            return null;
        }

        private DateTime AddHoursToDateTime(DateTime date, string hours)
        {
            return AddHoursToDateTime(date, Convert.ToInt32(hours));
        }

        private DateTime AddHoursToDateTime(DateTime date, int hours)
        {
            return date.AddHours(hours);
        }
        private DateTime GetTimeZoneDate(string timeZoneKey, DateTime dateToParse)
        {
            if (string.IsNullOrEmpty(timeZoneKey))
                return dateToParse.ToUniversalTime();

            TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneKey);
            var dateKind = DateTime.SpecifyKind(dateToParse, DateTimeKind.Unspecified);
            return TimeZoneInfo.ConvertTimeFromUtc(dateKind, timeZone);
        }

    }

    public enum MapperType
    {
        RMQMapper,
        DbAutoMapper
    }


    public class DbConverter
    {
        private static DateTime MinMssqlDateTimeValue { get; } = DateTime.Parse("1753-01-01");
        public Dictionary<Type, Func<dynamic, object>> Db { get; }

        public DbConverter(DbType type)
        {
            if (type == DbType.MYSQL)
                Db = new Dictionary<Type, Func<dynamic, object>>
            {
                { typeof(Guid), (value) => { return value is string ? new Guid(value).ToByteArray() : value == null ? null : value.ToByteArray(); } },
                { typeof(Int32), (value) => { return value is string ? Convert.ToInt32(value) : value; } },
                { typeof(Decimal), (value) => { return value is string ? Convert.ToDecimal(value) : value; } },
                { typeof(String), (value) => { return value; } },
                { typeof(DateTime), (value) => { return value; } }
            };
            //else if(type == DbType.MSSQL && Config.isNeedToConvertDateToGMT)
            //{
            //    { typeof(DateTime), (value) => { return value <= MinMssqlDateTimeValue ? null : value.ToUniversalTime(); } }
            //}
            else if (type == DbType.MSSQL && !Config.isNeedConvertTypesFromRMQ)
                Db = new Dictionary<Type, Func<dynamic, object>>
            {
                { typeof(byte[]), (value) => { return GetGuidFromByteArray(value); }},
                { typeof(String), (value) => { return value != null ? value : "" ; } },
                { typeof(Int32), (value) => { return value is string ? Convert.ToInt32(value) : value == null ? 0 : Convert.ToInt32(value); } },
                { typeof(UInt64), (value) => { return value is string ? Convert.ToInt32(value) : value == null ? 0 : Convert.ToInt32(value); } },
                { typeof(Decimal), (value) => { return value is string ? Convert.ToDecimal(value) : value == null ? 0M : Convert.ToDecimal(value); } },
                { typeof(Boolean), (value) => { return value is string ? Convert.ToBoolean(value) : value == null ? true : Convert.ToBoolean(value); } },
                { typeof(DateTime), (value) => { return value == null || value <= MinMssqlDateTimeValue ? null : value.ToUniversalTime(); } }
            };
            else if (type == DbType.MSSQL && Config.isNeedConvertTypesFromRMQ)  //For DBK 
                Db = new Dictionary<Type, Func<dynamic, object>>
            {
                { typeof(byte[]), (value) => { return  value is string ? GetGuidFromByteArray(Convert.FromBase64String(value)) : GetGuidFromByteArray(value); }},
                { typeof(DateTime), (value) => { return value <= MinMssqlDateTimeValue ? null : value.ToUniversalTime(); } }
            };
            else Db = new Dictionary<Type, Func<dynamic, object>>();
        }
        public Guid? GetGuidFromByteArray(dynamic value)
        {
            if (value == null)
                return null;
            var guid = new Guid(value);
            if (guid == Guid.Empty)
                return null;

            return guid;
        }
    }
}

