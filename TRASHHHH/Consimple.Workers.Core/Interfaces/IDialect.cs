﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Consimple.Workers.Interfaces
{
    public interface IDialect
    {
        string GetQuotedColumn(string columnName);
        string GetQuotedTable(string tableName);
        string SelectTopAllFrom(string tableName);
        string GetListFromSource(string tableName, List<string> columns);
        string Offset(int addedRecordsCount);
        string GetUpdateStatement(string tableName, List<string> columns, string primary = "Id", string condition = null);
        string GetInsertStatement(string tableName, List<string> columns);
        string GetCountByDateQuery(string systemSource);
    }
}
