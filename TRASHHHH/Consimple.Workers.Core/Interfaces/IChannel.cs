﻿using System;
using System.Collections.Generic;
using System.Text;

using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Consimple.Workers
{
    public interface IChannel
    {
        string Name { get; set; }        
        void Initloggerger(string directory);
        void Listen(IConnection connection = null);
    }
}
