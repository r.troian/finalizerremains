


IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[OrderProducts_INSERT]'))
DROP TRIGGER [dbo].[OrderProducts_INSERT]
go
	CREATE TRIGGER [dbo].[OrderProducts_INSERT]
	ON OrderProduct
	AFTER INSERT
	AS
	 DECLARE @Amount  DECIMAL(10,2),
			 @GiftCardPayedAmount  DECIMAL(10,2), 
			 @DiscountAmount  DECIMAL(10,2), 
			 @CouponsPayedAmount  DECIMAL(10,2)
        
			SELECT @Amount = Amount, 
				   @GiftCardPayedAmount = GiftCardPayedAmount, 
				   @DiscountAmount = DiscountAmount
				   FROM Inserted
	
			UPDATE OrderProduct
	set	TotalAmount = (@Amount - @GiftCardPayedAmount - @DiscountAmount)/100,
		PrimaryTotalAmount = (@Amount  - @GiftCardPayedAmount - @DiscountAmount ) /100
	where id = (SELECT Id
	FROM Inserted)

GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateOrderBonusesAmount]'))
DROP PROCEDURE [dbo].[CalculateOrderBonusesAmount]
go

CREATE PROCEDURE [dbo].[CalculateOrderBonusesAmount]  
@order_id uniqueidentifier
AS
	BEGIN
		DECLARE 
		    @bonusAmmount decimal(18, 2),
			@chargeBonusAmount decimal(18, 2),
			@writeOffBonusAmount decimal(18, 2),
			
			@TransactionTypeChargeId uniqueidentifier,
			@BonusTypeStatusId  uniqueidentifier

			SET @TransactionTypeChargeId ='C213208A-F36B-1410-5690-E0CB4EC6147E';
			SET @BonusTypeStatusId ='9CE731A2-F36B-1410-869F-E0CB4EC6147E';

		SET @bonusAmmount =  ISNULL((SELECT SUM(BaseBonusAmount) 
						 FROM [Transaction] 
						 WHERE OrderId = @order_id  
						 AND TypeId = @TransactionTypeChargeId
						 AND BonusTypeId = @BonusTypeStatusId), 0)

		SET @chargeBonusAmount =  ISNULL((SELECT SUM(BaseBonusAmount) 
						 FROM [Transaction] 
						 WHERE OrderId = @order_id
						 AND TypeId = @TransactionTypeChargeId
						 AND BonusTypeId <> @BonusTypeStatusId), 0)
			
		SET @writeOffBonusAmount =  ISNULL((SELECT SUM(BaseBonusAmount) 
						 FROM [Transaction] 
						 WHERE OrderId = @order_id 
						 AND  TypeId <> @TransactionTypeChargeId), 0)


			UPDATE [Order]
			SET 
				MoneyBonusAmount =   @chargeBonusAmount - @bonusAmmount, 
				BonusAmmount =  @bonusAmmount
			FROM [Order]  
			WHERE Id =   @order_id              
    
	END
GO
