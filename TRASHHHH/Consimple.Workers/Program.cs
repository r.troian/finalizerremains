﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using Microsoft.Extensions.Configuration;

namespace Consimple.Workers
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                InitConfig();

                //Close event
                InitEvents();




                //ChannelPool pool = InitPool();
                //pool.Start();
            }
            catch (Exception ex)
            {
                Config.logger.Fatal($"{ex.Source}: { ex.Message }");
                Config.logger.Warning("System: application will stop after 10 seconds...");
                Thread.Sleep(10000);
                Environment.Exit(0);
            }
        }

        private static void InitConfig()
        {
            Thread.CurrentThread.Name = "Main";
            Config.logger.Information("Application starting");
            //Config.InitConfig();

            //======================= new style ====================
            IConfigurationRoot configApp = Config.GetConf();
            Config.InitDatabases(configApp);
            Config.InitConfigRmqConnection(configApp);
            Config.InitConfig(configApp);
            Config.SyncList = Config.BuildSyncConfig();
            Config.InitChannelPoolConfig();
            Config.SchedulerTask();
            //======================================================

            Config.logger.Information("Check settings");

           //if (!Config.CheckSettings())
            //{
            //    Config.logger.Warning("System: application will stop after 10 seconds...");
            //    Thread.Sleep(10000);
            //    Environment.Exit(0);
            //}
        }
        private static ChannelPool InitPool()
        {
            Config.logger.Information("Init channel pool");
            var pool = new ChannelPool();
            var chanels = BuildChannels();
            pool.SetChannel(chanels);
            return pool;
        }
        private static List<IChannel> BuildChannels()
        {
            var channels = new List<IChannel>();
            foreach (var sync in Config.SyncList)
            {
                IChannel channel;

                if (sync.ChannelType == ChannelType.RmqChannel)
                    channel = new RMQChannel(sync);
                else
                    channel = new DbAutoChannel(sync);

                channel.Initloggerger(Config.loggerPath);
                channels.Add(channel);
            }

            return channels;
        }

        #region Close event  
        private static void InitEvents()
        {
            _handler += new EventHandler(Handler);
            SetConsoleCtrlHandler(_handler, true);
        }

        private static EventHandler _handler;

        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);

        private static bool Handler(CtrlType sig)
        {
            Config.UpdateConfig();

            Environment.Exit(-1);

            return true;
        }

        private delegate bool EventHandler(CtrlType sig);

        private enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_loggerOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }
        #endregion
    }
}
