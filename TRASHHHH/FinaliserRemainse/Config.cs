﻿using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.IO;
using System;


namespace Consimple.FinaliserRemainse
{
    public class  Config //: ConfigBase
    {
        public string directory;
        public string LogPath;

        public  IConfigurationRoot GetConf(string configFileName, string _directory = null)
        {
            _directory = _directory ?? directory;

            if (!File.Exists($"{_directory}\\{configFileName}"))
            {
                throw new Exception($"Config file is not exist or cannot open it here! Puth:[{_directory}\\{configFileName}]");
            }

            return new ConfigurationBuilder()
                .SetBasePath(_directory)
                .AddJsonFile(configFileName, optional: true, reloadOnChange: true)
                .Build();

            //return null;
        }

        public  void Init(string configFileName)
        {
            //var conf = GetConf(configFileName);
            //InitApp(conf);
        }


        public Config(string configFileName = "appConfig.json", string directoryPath = null)
        {
            directory = directoryPath?? Directory.GetCurrentDirectory();
            Init(configFileName);
        }


        protected void InitApp(IConfigurationRoot conf)
        {
            LogPath = conf.GetSection("settings")["logPath"] ?? directory;
        }

        public List<Sequence> GetSequenceList( string configFileName, string _directory = null)
        {
            var conf = GetConf(_directory, configFileName);
            return GetSequenceListByConfig(conf);
        }

        protected List<Sequence> GetSequenceListByConfig(IConfigurationRoot conf)
        {
            var sequenceList = new List<Sequence>();
            var section = conf.GetSection("SequenceList");
            var parseList = section.GetChildren();
            foreach (var item in parseList)
            {
                var sectionChild = new Sequence
                {
                    QueueType = item.GetValue<QueueType>("QueueType"),
                    QueueName = item.GetValue<string>("QueueName"),
                };
                sequenceList.Add(sectionChild);
            }
            return sequenceList;
        }
    }


    public class Sequence
    {
        public QueueType QueueType;
        public string QueueName;
    }

    public enum QueueType
    {
        WorkerSync,
        UpdateModifiedOn,
        UnwantedSql
    }
}
