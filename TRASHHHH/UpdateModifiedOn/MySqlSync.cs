﻿using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Linq;

namespace UpdateModifiedOn
{
    public class MySqlSync : DbProviderBase
    {
        public MySqlSync (string connectionString) : base(connectionString) {
            checkConnect();
        }


        private void checkConnect()
        {
            using (var context = new MySqlConnection(connectionString))
            {
                if (context.State == ConnectionState.Closed)
                    context.Open();
            }
        }

        public override IEnumerable<T> GetIdentifiers<T>(string query)
        {
            using (var context = new MySqlConnection(connectionString))
            {
                if (context.State == ConnectionState.Closed)
                    context.Open();

                return context.Query<T>(query, commandTimeout: 0);
            }
           
        }

        public override int UpdateEntriesById(IEnumerable<Guid> id, string table)
        {


            using (var context = new MySqlConnection(connectionString))
            {
                if (context.State == ConnectionState.Closed)
                    context.Open();
                string query = $"update {table} set ModifiedOn = UTC_TIMESTAMP() where Id IN (";
                foreach (var item in id)
                {
                    query += $" UuidToBin('{item}'),";
                }
                query = query.Substring(0, query.Length - 1);
                query += " );";

                return context.Execute(query);

               
            }
            
        }


    }
}
