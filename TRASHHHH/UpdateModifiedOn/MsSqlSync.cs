﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using System.Linq;

namespace UpdateModifiedOn
{
    public class MsSqlSync : DbProviderBase
    {
        public MsSqlSync(string connectionString) : base(connectionString) {
            checkConnect();
        }


        private void checkConnect()
        {
            using (var context = new SqlConnection(connectionString))
            {
                if (context.State == ConnectionState.Closed)
                    context.Open();   
            }
        }

        public override IEnumerable<T> GetIdentifiers<T>(string query)
        {
            using (var context = new SqlConnection(connectionString))
            {
                if (context.State == ConnectionState.Closed)
                    context.Open();

                return context.Query<T>(query, commandTimeout: 0);
            }
        }

        public override int UpdateEntriesById(IEnumerable<Guid> id, string table)
        {
            using (var context = new SqlConnection(connectionString))
            {
                if (context.State == ConnectionState.Closed)
                    context.Open();
                string query = $"update [{table}] set ModifiedOn = GETUTCDATE() where Id IN (";
                foreach (var item in id)
                {
                    query += $" '{item}',";
                }
                query = query.Substring(0, query.Length - 1);
                query += " );";

                return  context.Execute(query);
            }           
        }

    }
}
