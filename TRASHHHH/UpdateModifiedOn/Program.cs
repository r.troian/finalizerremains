﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.IO;


namespace UpdateModifiedOn
{
    class Program
    {
        public static string LogPath;
        static int packageSize;

        public static string perconaConnection;
        public static string bpmConnection;

        public static bool saveToFile;
        public static List<TablesList> TablesList;
        public static int countUpdateError;


        private static string directory => Directory.GetCurrentDirectory();
        public static Serilog.Core.Logger Log = Logger.InitConfiguration(directory);

        static  void  Main(string[] args)
        {
            bool savingToFile = false;
            InitConfig();
            Log.Information($"Config Inited TablesList[{TablesList.Count()}]");
            Log.Information($"saveToFile - [{saveToFile}]");
            Console.WriteLine("");

            MySqlSync perconaProvider;
            MsSqlSync msProvider;

            try
            {
               perconaProvider = new MySqlSync(perconaConnection);
               msProvider = new MsSqlSync(bpmConnection);
            }
            catch (Exception ex)
            {
                Log.Fatal($"Uncorrect ConnectionString Err: message[{ex.Message}]" );
                Console.WriteLine("For Exit please press any key!");
                Console.ReadKey();
                throw ex;
            }


            foreach (var item in TablesList)
            {

                Log.Information($"SyncDirect - {item.SyncDirect.ToString()}");
                Log.Information($"Action - {item.Action.ToString()}");
                IEnumerable<DataModel> msData = null;
                IEnumerable<DataModel> myData = null;
                IEnumerable<DataModel> missingDataInBpm = null;
                IEnumerable<DataModel> missingDataInPercona = null;

                IEnumerable<Guid> missingDataInBpm_Id = new List<Guid>();
                IEnumerable<Guid> missingDataInPercona_Id = new List<Guid>();

                string extendedBpmColumnName = string.IsNullOrEmpty(item.ExtendedBpmColumnName) ? "NULL" : item.ExtendedBpmColumnName;
                string extendedPerconaColumnName = string.IsNullOrEmpty(item.ExtendedPerconaColumnName) ? "NULL" : item.ExtendedPerconaColumnName;
                Log.Information($"ExtendedColumnName - BPM.{item.BpmTable}[{extendedBpmColumnName}]");
                Log.Information($"ExtendedColumnName - Percona.{item.PerconaTable}[{extendedPerconaColumnName}]");

                if (item.SyncDirect == SyncDirect.bpmToPercona)
                {
                    msData = msProvider.GetIdentifiers<DataModel>(
                        $"SELECT Id as Id, {extendedBpmColumnName} as ExtendedColumn FROM [{item.BpmTable}]");
                    Log.Information($"BPM table - '{item.BpmTable}' count = [{msData.Count()}]");

                    myData = perconaProvider.GetIdentifiers<DataModel>(
                          $"SELECT Id as Id, {extendedPerconaColumnName} as ExtendedColumn FROM {item.PerconaTable}");
                    Log.Information($"Percona table - '{item.PerconaTable}' count = [{ myData.Count()}]");
                }
                else
                {
                    myData = perconaProvider.GetIdentifiers<DataModel>(
                        $"SELECT Id as Id, {extendedPerconaColumnName} as ExtendedColumn FROM {item.PerconaTable}");
                    Log.Information($"Percona table - '{item.PerconaTable}' count = [{ myData.Count()}]");

                    msData = msProvider.GetIdentifiers<DataModel>(
                        $"SELECT Id, {extendedBpmColumnName} as ExtendedColumn FROM [{item.BpmTable}]");
                    Log.Information($"BPM table - '{item.BpmTable}' count = [{msData.Count()}]");
                }


                if ((item.Action != Action.analysisPercona && item.Action != Action.update) || item.SyncDirect != SyncDirect.bpmToPercona )
                {
                    missingDataInBpm = Compare(myData, msData);
                    missingDataInBpm_Id = GetIdFromDataModel(missingDataInBpm);
                    Log.Information($"Detected missing data in BPM - [{missingDataInBpm.Count()}]");
                }

                if ((item.Action != Action.analysisBpm && item.Action != Action.update) || item.SyncDirect != SyncDirect.perconaToBpm)
                {
                    missingDataInPercona = Compare(msData, myData);
                    missingDataInPercona_Id = GetIdFromDataModel(missingDataInPercona);
                    Log.Information($"Detected missing data in Percona - [{missingDataInPercona.Count()}]");
                }

                if (saveToFile)
                {
                    if( (missingDataInPercona_Id != null && missingDataInPercona_Id.Count() > 0)
                        || (missingDataInBpm_Id != null && missingDataInBpm_Id.Count() > 0))
                    {
                        savingToFile = true;
                    }
                    ToFile(missingDataInBpm_Id, "Percona", item.PerconaTable, item.ExtendedPerconaColumnName);
                    ToFile(missingDataInPercona_Id, "BPM", item.BpmTable, item.ExtendedBpmColumnName);
                }

                if (item.Action == Action.update)
                {
                    if (item.SyncDirect == SyncDirect.perconaToBpm || item.SyncDirect == SyncDirect.duplex)
                    {
                        int resUpdatePercona = Update(missingDataInBpm_Id, perconaProvider, item.PerconaTable);
                        Log.Information($"Update row in [PerconaTables-'{item.PerconaTable}'] - [{resUpdatePercona}]");
                    }

                    if (item.SyncDirect == SyncDirect.bpmToPercona || item.SyncDirect == SyncDirect.duplex)
                    {
                        int resUpdateBpm = Update(missingDataInPercona_Id, msProvider, item.BpmTable);
                        Log.Information($"Update row in [BpmTables-'{item.BpmTable}'] - [{resUpdateBpm}]");
                    }
                }
              
                Console.WriteLine("");
            }

            if (savingToFile)
            {
                Console.WriteLine("Kostyl waite save to file (Dont press any key during seved to file)");
            }
            else
            {
                Console.WriteLine("DONE!\nFor Exit please press any key!");
            }


            
            Console.ReadKey();
        }


        private static IEnumerable<Guid> GetIdFromDataModel(IEnumerable<DataModel> md)
        {
            var res = new List<Guid>();

            foreach (var it in md)
            {
                res.Add(
                    (Guid)it.Id
                ) ;
            }

            return res;
        }


        private static async void ToFile(IEnumerable<Guid> Ids, string systemName, string tatableName, string extendedColumnName)
        {

            if(Ids == null || Ids.Count() == 0) { return; }

            extendedColumnName = string.IsNullOrEmpty(extendedColumnName) ? "" : $"[{extendedColumnName}]_";
            Log.Information($"Saving '{tatableName}_{systemName}_{extendedColumnName}Ids'");

            string writePath = $@"{LogPath}\\{tatableName}_{systemName}_{extendedColumnName}Ids.txt";

            try
            {
                using (StreamWriter sw = File.CreateText(writePath))// new StreamWriter(writePath, false, System.Text.Encoding.Default))
                {
                    await sw.WriteLineAsync($"{DateTime.Now}  | UTC - {DateTime.UtcNow}  | Count - {Ids.Count()}");

                    foreach(var item in Ids)
                    {
                        if(systemName == "Percona")
                        {
                            await sw.WriteLineAsync($" UuidToBin('{item}'),");
                        }
                        else
                        {
                            await sw.WriteLineAsync($" '{item}',");
                        }
                       
                    }
                }

                Log.Information($"Saved to '{writePath}'");
            }
            catch (Exception e)
            {
                Log.Error($"Invalid save to {writePath} Err: message [{e.Message}]");
            }

        }

        private static IEnumerable<T> Compare<T>(IEnumerable<T> first, IEnumerable<T> second)
        {
            return first.Except(second);
        }

        private static int Update(IEnumerable<Guid> Ids, DbProviderBase BbProvider, string table)
        {
            int result = 0;
            int counter = 0;
            int pack = packageSize;

            if (Ids == null) { return -1; }


            while (counter < Ids.Count())
            {
                if(counter + packageSize > Ids.Count())
                {
                    pack = Ids.Count() - counter;
                }

                //result += BbProvider.UpdateEntriesById(Ids.Skip(counter).Take(pack), table);
                int countError = countUpdateError; // 3;
                result += updater(Ids.Skip(counter).Take(pack), BbProvider, table, ref countError);
                Console.WriteLine($"{DateTime.Now} data - [{result}]");
                counter += packageSize;
            }
        
            return result;
        }

        private static int updater(IEnumerable<Guid> id, DbProviderBase BbProvider,  string table, ref int countError)
        {
            if (id == null || id.Count() == 0) return 0;

            int result = 0;
            try
            {
                result = BbProvider.UpdateEntriesById(id, table);
            }
            catch (Exception ex)
            {
                if(countError <= 0)
                {
                    Log.Fatal($"UPDATE Err: message[{ex.Message}]\n{ex.StackTrace}");
                    Console.WriteLine("For Exit please press any key!");
                    Console.ReadKey();
                    throw ex;
                }
                Log.Error($"Error UPDATE N({countError}) [{ex.Message}]\n{ex.StackTrace}");
                countError--;
                System.Threading.Thread.Sleep(30000); //30 sec
                result = updater(id, BbProvider, table, ref countError);
            }

            return result;
        }

        public static void InitConfig()
        {
            var conf = GetConf();

            try
            {
                LogPath = conf.GetSection("settings")["logPath"] ?? directory;
                Log = Logger.InitConfiguration(LogPath);
                packageSize = Convert.ToInt32(conf.GetSection("settings")["maxPackageSize"]);
                countUpdateError = Convert.ToInt32(conf.GetSection("settings")["countUpdateError"]);
                saveToFile = Convert.ToBoolean(conf.GetSection("settings")["saveToFile_Ids"]);
                perconaConnection = conf.GetSection("connectionStrings")["perconaConnection"];
                bpmConnection = conf.GetSection("connectionStrings")["bpmConnection"];

                TablesList = new List<TablesList>();
                var section = conf.GetSection("tablesList");
                var parseList = section.GetChildren();
                foreach (var item in parseList)
                {
                    var tab = new TablesList
                    {
                        PerconaTable  = item.GetValue<string>("perconaTable"),
                        BpmTable = item.GetValue<string>("bpmTable"),
                        ExtendedBpmColumnName = item.GetValue<string>("extendedBpmColumnName"),
                        ExtendedPerconaColumnName = item.GetValue<string>("extendedPerconaColumnName"),
                        Action = item.GetValue<Action>("action"),
                        SyncDirect = item.GetValue<SyncDirect>("syncDirect")
                    };
                    TablesList.Add(tab);
                }             
            }
            catch (Exception ex)
            {
                Log.Fatal($"System: Invalid appConfig.json OR ancorrect any parameter(s) in appConfig.json");
                throw ex;
            }

        }

        private static IConfigurationRoot GetConf()
        {
            return new ConfigurationBuilder()
                .SetBasePath(directory)
                .AddJsonFile($"appConfig.json", optional: true, reloadOnChange: true)
                .Build();
        }

    }


    public class TablesList
    {
        public string PerconaTable { get; set; }
        public string BpmTable { get; set; }
        public string ExtendedBpmColumnName { get; set; }
        public string ExtendedPerconaColumnName { get; set; }
        public Action Action { get; set; }
        public SyncDirect SyncDirect { get; set; }
    }

    public enum SyncDirect
    {
        bpmToPercona,
        perconaToBpm,
        duplex
    }

    public enum Action
    {
        update, 
        analysis, 
        analysisBpm, 
        analysisPercona
    }



}
