﻿using System.Threading;

using Serilog;
using Serilog.Events;
using Serilog.Core;

namespace UpdateModifiedOn
{
    public static class Logger
    {
        private static object obj = new object();
        public static Serilog.Core.Logger InitConfiguration(string directory)
        {
            return new LoggerConfiguration()
                .Enrich.With(new ThreadIdEnricher())
                .MinimumLevel.Debug()
                .WriteTo.ColoredConsole(restrictedToMinimumLevel: LogEventLevel.Information)
                .WriteTo.File($"{directory}\\logs\\system-log-.txt", rollingInterval: RollingInterval.Day,
                    outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}")
                .CreateLogger();
        }

        public static void LogError(this Serilog.Core.Logger logger, string queue, string message)
        {
            lock(obj)
            {
                logger.Error($"=> SyncBpmProcessing_{ queue }: {message}");    
            }
        }

        public static void LogInfo(this Serilog.Core.Logger logger, string queue, string message)
        {
            lock (obj)
            {
                logger.Information($"=> SyncBpmProcessing_{ queue }: {message}");
            }
        }

        public static Serilog.Core.Logger InitConfiguration(string fileName, string directory)
        {
            return new LoggerConfiguration()
                .Enrich.With(new ThreadIdEnricher())
                .MinimumLevel.Debug()
                .WriteTo.ColoredConsole(restrictedToMinimumLevel: LogEventLevel.Information)
                .WriteTo.File($"{directory}\\logs\\{fileName}-log-.txt", rollingInterval: RollingInterval.Day,
                    outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}")
                .CreateLogger();
        }
    }

    public class ThreadIdEnricher : ILogEventEnricher
    {
        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            logEvent.AddPropertyIfAbsent(propertyFactory.CreateProperty(
                    "ThreadId", Thread.CurrentThread.ManagedThreadId));
        }
    }
}
