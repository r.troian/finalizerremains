﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UpdateModifiedOn
{
    public abstract class DbProviderBase
    {
        protected string connectionString;
        public DbProviderBase(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public abstract IEnumerable<T> GetIdentifiers<T>(string query);

        public abstract int UpdateEntriesById(IEnumerable<Guid> id, string table);
    }
}
