﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UpdateModifiedOn
{
    class DataModel
    {
        private Guid id;
        private object extendedColumn;
        public object Id 
        { 
           
            set
            {
                if(value is Guid){
                    id = (Guid)value;
                }

                if (value is byte[])
                {
                    id = new Guid((byte[])value);
                }
                
            }
            get { return id; }
        }
        public object ExtendedColumn 
        {
            set
            {
                if (value is byte[])
                {
                    extendedColumn = new Guid((byte[])value);
                }
                else
                {
                    extendedColumn = value;
                }
            }
            get { return extendedColumn; }
        }


        public override int GetHashCode()
        {
            return id.GetHashCode() + (extendedColumn != null ? extendedColumn.GetHashCode() : 0);
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            DataModel dm = obj as DataModel;
            if (dm as DataModel == null)
                return false;

            if(dm.ExtendedColumn == null && this.ExtendedColumn == null)
            {
                return (Guid)dm.Id == (Guid)this.Id;
            }

            if((Guid) dm.Id == (Guid)this.Id){
                this.Id = this.Id;
            }

            //var typedm = dm.ExtendedColumn.GetType();
            //var typeThis = this.ExtendedColumn.GetType();

            //bool alpha1 = (Guid)dm.Id == (Guid)this.Id;
            //bool alpha2 = this.ExtendedColumn != null;
            //bool alpha3 = dm.ExtendedColumn != null;
            //bool alpha4 = dm.ExtendedColumn.GetType() == this.ExtendedColumn.GetType();            
            //bool alpha5 = dm.extendedColumn == this.extendedColumn;
            //bool alpha51 = dm.extendedColumn.GetType() == typeof(Guid) || dm.extendedColumn.GetType() == typeof(DateTime);
            //if (alpha51)
            //{
            //    alpha5 = dm.extendedColumn.ToString() == this.extendedColumn.ToString();
            //}
            //return alpha1 && alpha2 && alpha3 && alpha4 && alpha5;

            return (Guid)dm.Id == (Guid)this.Id
                   && this.ExtendedColumn != null
                   && dm.ExtendedColumn != null
                   && dm.ExtendedColumn.GetType() == this.ExtendedColumn.GetType()
                   && dm.ExtendedColumn.ToString() == this.ExtendedColumn.ToString();
        }

    }
}
