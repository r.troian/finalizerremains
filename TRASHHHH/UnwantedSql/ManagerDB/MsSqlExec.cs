﻿
using Dapper;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Linq;



namespace UnwantedSql
{
    class MsSqlExec : DbProviderBase
    {
        public static  int constCountError = 3;
        public static int constwatchDogTime = 30000;

        
        private int _countError;
        public static int _watchDogTime;

        public MsSqlExec(string connectionString, int? countError = null, int? watchDogTime = null) : base(connectionString)
        {
            _countError = countError ?? constCountError;
            _watchDogTime = watchDogTime ?? constwatchDogTime;
            checkConnect();
        }

        protected override bool checkConnect()
        {
            using (var context = new SqlConnection(connectionString))
            {
                if (context.State == ConnectionState.Closed)
                    context.Open();

                if (context.State == ConnectionState.Open)
                    return true;
            }

            return false;
        }


        public override T ExecuteScalar<T>(string sql, object param = null, IDbTransaction transaction = null,  int? commandTimeout = null, CommandType? commandType = null)
        {
            using (var context = new SqlConnection(connectionString))
            {
                if (context.State == ConnectionState.Closed)
                    context.Open();

                return context.ExecuteScalar<T>(sql, param, transaction, commandTimeout, commandType);
            }
        }

        public override IEnumerable<T> Query<T>(string sql, object param = null, IDbTransaction transaction = null, bool buffered = true, int? commandTimeout = null, CommandType? commandType = null)
        {
            using (var context = new SqlConnection(connectionString))
            {
                if (context.State == ConnectionState.Closed)
                    context.Open();
                return context.Query<T>(sql, param, transaction, buffered, commandTimeout, commandType);
            }
        }

        public override int Exec(string sql, object  param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            using (var context = new SqlConnection(connectionString))
            {
                if (context.State == ConnectionState.Closed)
                    context.Open();
                
                return context.Execute(sql, param, transaction,  commandTimeout, commandType);
            }
        }

        public override int ExecTry(Serilog.Core.Logger logger, string sql, int? countError = null, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            countError = countError ?? _countError;
            int result = 0;
            try
            {
                result = Exec(sql, param, transaction, commandTimeout, commandType);
            }
            catch (Exception ex)
            {
                if (countError <= 0)
                {                             
                    throw ex;
                }
                logger.Error($"Error exec({countError - --countError}) SQL:[{sql}]  \n Message:[{ex.Message}]\n StackTrace:[{ex.StackTrace}]");
                
                System.Threading.Thread.Sleep(_watchDogTime); //30 sec
                result = ExecTry(logger, sql, countError, param, transaction, commandTimeout, commandType);
            }

            return result;
        }


    }
}
