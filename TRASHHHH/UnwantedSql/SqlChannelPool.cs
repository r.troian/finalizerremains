﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace UnwantedSql
{
    public class SqlChannelPool : SqlChannelBase
    {
        public static string constMsConnectinString;
        public static string constMyConnectinString;

        public SqlChannelPool(Sql sql, string directoryPathForConfig)
        {
            _sql = sql;
            _directory = directoryPathForConfig;
            _sqlContext = sql.SqlContext;
            Name = sql.Queue;
            DbType = sql.DbType;
            SqlType = sql.SqlType;
            SqlText = GetSqlTextByContext(sql.SqlContext);
            CommandTimeout = sql.CommandTimeout;
        }

        private string GetSqlTextByContext(string sqlContext)
        {
            var contextArr = sqlContext.Split('.');

            if(contextArr.Length > 1)
            {
                string extension = contextArr[contextArr.Length - 1];

                switch (extension)
                {
                    case "txt": { sqlContext = GetSqlTextFromFile($"{_directory}\\SQL\\{sqlContext}" ); } break;
                    case "sql": { sqlContext = GetSqlTextFromFile($"{_directory}\\SQL\\{sqlContext}", Encoding.UTF8); } break;
                    default : { } break;
                }
            }
            
            return sqlContext;
        }
        private string GetSqlTextFromFile(string path, Encoding enc = null)
        {
            if (File.Exists(path))
            {
                string SqlText = "";
                string[] readText;
                if (enc != null)
                {
                    readText = File.ReadAllLines(path, enc);
                }
                else
                {
                   readText = File.ReadAllLines(path);
                }

                foreach (string s in readText)
                {
                    SqlText += s;
                }
                return SqlText;
            }
            else
            {
                throw new Exception($"\nFile not exist or cannot open it here - path:[{path}]\nEncoding:[{enc}]\n");
            }
        }
        
        public override int Apply(string connectinString = null,  DbProviderBase sqlExec = null)
        {
            if(sqlExec == null)
            {

                if(DbType == DbType.MSSQL)
                {
                    connectinString = connectinString ?? Config.constMsConnectinString;
                    sqlExec = new MsSqlExec(connectinString);
                }
                else if(DbType == DbType.MYSQL)
                {
                    connectinString = connectinString ?? Config.constMyConnectinString;
                    sqlExec = new MySqlExec(connectinString);
                }
                else
                {
                    throw new Exception($"Uncnow DbType : [{DbType}]");  
                }
            }


            if(SqlType == SqlType.Exec)
            {
                try
                {
                    return sqlExec.Exec(SqlText, commandTimeout : CommandTimeout);       
                }
                catch(Exception ex)
                {
                    logger.Error($"SQL Error Exec SQL:[{_sqlContext}]  \n Message:[{ex.Message}]\n StackTrace:[{ex.StackTrace}]");
                    throw new SqlException(ex.Message);
                }
            }
            else if (SqlType == SqlType.Count)
            {
                try
                {
                    return sqlExec.ExecuteScalar<int>(SqlText, commandTimeout: CommandTimeout);
                }
                catch (Exception ex)
                {
                    logger.Error($"SQL Error ExecCount SQL:[{_sqlContext}]  \n Message:[{ex.Message}]\n StackTrace:[{ex.StackTrace}]");
                    throw new SqlException(ex.Message);
                }
            }
            else
            {
                return -1;
            }

        }

    }
}
