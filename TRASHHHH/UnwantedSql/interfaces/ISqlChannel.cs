﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnwantedSql
{
    interface ISqlChannel
    {
        string Name { get; }
        void InitLogger(string directory);
        int Apply(string connectinString = null, DbProviderBase sqlExec = null);
    }
}
