﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace UnwantedSql
{
    public class UnwantedSql
    {
        private List<Sql> SqlList = new List<Sql>();
        public List<SqlChannelPool> SqlChannelList = new List<SqlChannelPool>(); //ISqlChannel

        public string directoryPathForLogger;
        public string directoryPathForConfig;

        public UnwantedSql(string _directoryPathForLogger = null, string _directoryPathForConfig = null)
        {
            directoryPathForLogger = _directoryPathForLogger?? Config.LogPath;
            directoryPathForConfig = _directoryPathForConfig?? Config.directory;

            ConfigInit(directoryPathForConfig);
            SqlChannelList = BuildChannels(directoryPathForLogger, directoryPathForConfig);
        }

        private void ConfigInit(string directoryPathForConfig)
        {
            var conf = Config.GetConf(directoryPathForConfig, "SqlList.json");
            SqlList = Config.GetSqlList(conf);
        }
        public SqlChannelPool GetSqlChannelByQueueName(string queueName)
        {
            return SqlChannelList.Find(x => x.Name == queueName);
        }
        private  List<SqlChannelPool> BuildChannels(string directoryPathForLogger, string directoryPathForConfig )
        {        
            var channels = new List<SqlChannelPool>();
            foreach (var sql in SqlList)
            {
                SqlChannelPool channel = new SqlChannelPool(sql, directoryPathForConfig);              
                channel.InitLogger(directoryPathForLogger);// Config.LogPath);
                channels.Add(channel);
            }
            return channels;
        }

    }
}
