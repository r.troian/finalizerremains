﻿using System.Collections.Generic;
using System.IO;
using Microsoft.Extensions.Configuration;

using System;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.Collections.Specialized;
using System.Globalization;


namespace UnwantedSql
{
    public static class Config
    {
        public static string constMsConnectinString;
        public static string constMyConnectinString;
        public static string directory => Directory.GetCurrentDirectory();
        public static string LogPath;


        public static void Init()
        {
            var conf = GetConf();
            InitApp(conf);
        }

        public static void InitApp(IConfigurationRoot conf)
        {
            LogPath = conf.GetSection("settings")["logPath"] ?? directory;
            constMsConnectinString = conf.GetSection("connectionStrings")["bpmConnection"];
            constMyConnectinString = conf.GetSection("connectionStrings")["perconaConnection"];
        }
                                                                                            
        public static IConfigurationRoot GetConf(string _directory = null, string configFileName = "appConfig.json")
        {
            _directory = _directory ?? directory;

            if (!File.Exists($"{_directory}\\{configFileName}"))
            {
                throw new Exception($"Config file is not exist or cannot open it here! Puth:[{directory}\\{configFileName}]");
            }

            return new ConfigurationBuilder()
                .SetBasePath(directory)
                .AddJsonFile(configFileName, optional: true, reloadOnChange: true)
                .Build();
        }

        public static List<Sql> GetSqlList(IConfigurationRoot conf)
        {
            var sqlList = new List<Sql>();
            var section = conf.GetSection("SqlList");
            var parseList = section.GetChildren();
            foreach (var item in parseList)
            {
                var sectionChild = new Sql
                {
                    Queue = item.GetValue<string>("Queue"),
                    DbType = item.GetValue<DbType>("DbType"),
                    SqlType = item.GetValue<SqlType>("SqlType"),
                    SqlContext = item.GetValue<string>("SqlContext"),
                    CommandTimeout = item.GetValue<int>("CommandTimeout") 
                };
                sqlList.Add(sectionChild);
            }
            return sqlList;
        }


    }



}
