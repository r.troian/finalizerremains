﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnwantedSql
{
    public class SqlException : Exception
    {
        public SqlException(string message) : base(message)
        {

        }
    }
    public class Sql
    {
        public string Queue;
        public SqlType SqlType;
        public DbType DbType;
        public string SqlContext;
        public int? CommandTimeout;
    }


    public enum SqlType
    {
        Exec,
        Count
        //    ,
        //Select,
        //Dml
    }

    public enum DbType
    {
        MYSQL,
        MSSQL,
        POSTEGRESSQL
    }


}
