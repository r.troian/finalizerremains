﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnwantedSql
{
    public abstract class SqlChannelBase : ISqlChannel
    {

        protected Sql _sql;
        protected string _sqlContext;
        public string _directory;
        protected Serilog.Core.Logger logger;
        public DbType DbType { get; set; }
        public string Name { get;  set; }
        public SqlType SqlType { get;  set; }
        public string SqlText { get;  set; }
        public int? CommandTimeout { get; set; }

   


        public void InitLogger(string directoryPathForLogger)
        {
            //this.directory = directory;
            logger = Logger.InitConfiguration(Name, directoryPathForLogger);
        }

        public abstract int Apply(string connectinString = null, DbProviderBase sqlExec = null);

    }
}
