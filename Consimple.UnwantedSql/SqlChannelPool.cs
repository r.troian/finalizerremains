﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace Consimple.UnwantedSql
{
    public class SqlChannelPool : ChannelPoolBase
    {
        public string constMsConnectinString;
        public string constMyConnectinString;

        protected string _sqlContext;
        public string _directory;
        public DbType DbType { get; set; }
        public SqlType SqlType { get; set; }
        public string SqlText { get; set; }
        public int? CommandTimeout { get; set; }

        private DbProviderBase SqlExec;
        public SqlChannelPool(Sql sql, Config config)
        {
            constMsConnectinString = config.constMsConnectinString;
            constMyConnectinString = config.constMyConnectinString;
            _directory = config.directory;

               
            _sqlContext = sql.SqlContext;

            Name = sql.Queue;
            DbType = sql.DbType;
            SqlType = sql.SqlType;
            SqlText = GetSqlTextByContext(sql.SqlContext);
            CommandTimeout = sql.CommandTimeout;

            InitLogger(config.LogPath);
            InitDb();
        }

        private void InitDb()
        {
            if (DbType == DbType.MSSQL)
            {              
                SqlExec = new MsSqlExec(constMsConnectinString);
            }
            else if (DbType == DbType.MYSQL)
            {
                SqlExec = new MySqlExec(constMyConnectinString);
            }
            else
            {
                throw new Exception($"Uncnow DbType : [{DbType}]");
            }
        }

        private string GetSqlTextByContext(string sqlContext)
        {
            var contextArr = sqlContext.Split('.');

            if(contextArr.Length > 1)
            {
                string extension = contextArr[contextArr.Length - 1];

                switch (extension)
                {
                    case "txt": { sqlContext = GetSqlTextFromFile($"{_directory}\\SQL\\{sqlContext}" ); } break;
                    case "sql": { sqlContext = GetSqlTextFromFile($"{_directory}\\SQL\\{sqlContext}", Encoding.UTF8); } break;
                    default : { } break;
                }
            }
            
            return sqlContext;
        }
        private string GetSqlTextFromFile(string path, Encoding enc = null)
        {
            if (File.Exists(path))
            {
                string SqlText = "";
                string[] readText;
                if (enc != null)
                {
                    readText = File.ReadAllLines(path, enc);
                }
                else
                {
                   readText = File.ReadAllLines(path);
                }

                foreach (string s in readText)
                {
                    SqlText += s;
                }
                return SqlText;
            }
            else
            {
                throw new Exception($"\nFile not exist or cannot open it here - path:[{path}]\nEncoding:[{enc}]\n");
            }
        }


        public override Dictionary<string, object> Apply()
        {
            var res = new Dictionary<string, object>();            
            string objName = $"UnwantedSql - Queue: [{Name}], DbType: [{DbType}], SqlType: [{SqlType}]"; 
            int objValue = ApplyListen();
            logger.Information(objName);
            logger.Information($"Sql: [{_sqlContext}] Return: [{objValue}]");
            res.Add(objName, objValue);
            return res;
        }

        private  int ApplyListen()
        {
           
            if(SqlType == SqlType.Exec)
            {
                try
                {
                    return SqlExec.Exec(SqlText, commandTimeout : CommandTimeout);       
                }
                catch(Exception ex)
                {
                    logger.Error($"SQL Error Exec SQL:[{_sqlContext}] \nMessage:[{ex.Message}]\nStackTrace:[{ex.StackTrace}]");
                    throw new SqlException(ex.Message);
                }
            }
            else if (SqlType == SqlType.Count)
            {
                try
                {
                    return SqlExec.ExecuteScalar<int>(SqlText, commandTimeout: CommandTimeout);
                }
                catch (Exception ex)
                {
                    logger.Error($"SQL Error ExecCount SQL:[{_sqlContext}]  \n Message:[{ex.Message}]\n StackTrace:[{ex.StackTrace}]");
                    throw new SqlException(ex.Message);
                }
            }
            else
            {
                return -1;
            }

        }

    }
}
