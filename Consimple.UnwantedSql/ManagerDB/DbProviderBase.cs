﻿
using Dapper;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Linq;

namespace Consimple.UnwantedSql
{
    public abstract class DbProviderBase 
    {

        protected string connectionString;
        public DbProviderBase(string connectionString)
        {
            this.connectionString = connectionString;
        }

        protected abstract bool checkConnect();
        public abstract IEnumerable<T> Query<T>(string sql, object param = null, IDbTransaction transaction = null, bool buffered = true, int? commandTimeout = null, CommandType? commandType = null);
        public abstract int Exec(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        public abstract int ExecTry(Serilog.Core.Logger logger, string sql, int? countError = null, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        public abstract T ExecuteScalar<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);


        public abstract int UpdateModifiedOnById(IEnumerable<Guid> id, string table, Serilog.Core.Logger logger, int countErrorTry);
        public abstract int DeleteById(IEnumerable<Guid> id, string table, Serilog.Core.Logger logger, int countErrorTry);

        //public abstract IEnumerable<T> GetIdentifiers<T>(string query);

        //public abstract int UpdateEntriesById(IEnumerable<Guid> id, string table);
    }
}
