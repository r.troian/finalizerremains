﻿using System.Collections.Generic;
using System.IO;
using Microsoft.Extensions.Configuration;

using System;



namespace Consimple.UnwantedSql
{
    public class Config : ConfigBase
    {
        public  string constMsConnectinString;
        public  string constMyConnectinString;

        public Config(string configFileName = "appConfig.json", string directoryPath = null)
        {
            directory = directoryPath ?? Directory.GetCurrentDirectory();
            Init(configFileName);
        }

        protected override void InitApp(IConfigurationRoot conf)
        {
            LogPath = conf.GetSection("settings")["loggerPath"] ?? directory;
            constMsConnectinString = conf.GetSection("connectionStrings")["bpmConnection"];
            constMyConnectinString = conf.GetSection("connectionStrings")["perconaConnection"];
        }

        public List<Sql> GetSqlList(string configFileName, string _directory = null)
        {
            var conf = GetConf(configFileName, _directory);
            return GetSqlListByConf(conf);
        }

        protected  List<Sql> GetSqlListByConf(IConfigurationRoot conf)
        {
            var sqlList = new List<Sql>();
            var section = conf.GetSection("SqlList");
            var parseList = section.GetChildren();
            foreach (var item in parseList)
            {
                var sectionChild = new Sql
                {
                    Queue = item.GetValue<string>("Queue"),
                    DbType = item.GetValue<DbType>("DbType"),
                    SqlType = item.GetValue<SqlType>("SqlType"),
                    SqlContext = item.GetValue<string>("SqlContext"),
                    CommandTimeout = item.GetValue<int>("CommandTimeout") 
                };
                sqlList.Add(sectionChild);
            }
            return sqlList;
        }
    }

    public class SqlException : Exception
    {
        public SqlException(string message) : base(message) { }
    }

    public class Sql
    {
        public string Queue;
        public SqlType SqlType;
        public DbType DbType;
        public string SqlContext;
        public int? CommandTimeout;
    }


    public enum SqlType
    {
        Exec,
        Count
        //    ,
        //Select,
        //Dml
    }

    public enum DbType
    {
        MYSQL,
        MSSQL,
        POSTEGRESSQL
    }

}
