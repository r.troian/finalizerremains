﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Consimple.UnwantedSql
{
    public class UnwantedSql
    {
        public Config config;
        private List<Sql> SqlList;
        public List<IChannelPool> SqlChannelList;
        public UnwantedSql()
        {
            config = new Config();
            SqlList = config.GetSqlList("SqlList.json");
            SqlChannelList = BildChannelPool(SqlList, config);
        }

        public IChannelPool GetSqlChannelByQueueName(string queueName)
        {
            return SqlChannelList.Find(x => x.Name == queueName);
        }
        private List<IChannelPool> BildChannelPool(List<Sql> SqlList, Config config)
        {
            var res = new List<IChannelPool>();
            foreach(var sql in SqlList)
            {
                res.Add(new SqlChannelPool(sql, config));
            }

            if (res.ChackNotDistinctName())
            {
                throw new Exception($"SqlList mast contain only unic QueueName: [{string.Join(", ", res.GetNotDistinctName())}]");
            }
            return res;
        }



    }
}
