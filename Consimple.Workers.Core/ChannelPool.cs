﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Consimple.Workers
{
    public class ChannelPool
    {
        private List<IChannel> _channels;
        public static List<Thread> appThreads;
        public ChannelPool()
        {
            _channels = new List<IChannel>();
        }

        public List<IChannel> GetChannelList()
        {
            return _channels;
        }

        public void SetChannel(IChannel channel)
        {
            if (channel == null)
                return;

            _channels.Add(channel);
        }

        public void SetChannel(List<IChannel> channels)
        {
            if (channels.IsNullOrEmpty())
                return;

            _channels.AddRange(channels);
        }

        public void Start()
        {
            try
            {
                foreach (var channel in _channels)
                {
                    var thread = new Thread(() => channel.Listen(Config.RmqConnection.GetConnection()))
                    {
                        Name = channel.Name
                    };

                    thread.Start();
                }
            }
            catch (Exception ex)
            {
              var a =   ex.Message;
            }
        }
    }
}
