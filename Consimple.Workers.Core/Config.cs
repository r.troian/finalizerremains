﻿using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Quartz;
using Quartz.Impl;
using System.Collections.Specialized;
using System.Globalization;

namespace Consimple.Workers
{
    public static class Config
    {
        public static readonly string SyncListPath = directory + "\\synchronizationList.json";
        public static string LogPath;
        public static string procedureName = "";
        public static bool isNeedConvertTypesFromRMQ = false;
        public static bool isNeedToConvertDateToGMT = true;
        public static string tableForLostOrders = "LostOrder";
        public static IsoDateTimeConverter dateTimeConverter;
        public static int MaxSelectCount = 100000;
        public static int MaxInsertAttempts = 1;
        public static int ToSelectPrevDataSeconds = 360;
        public static TimeSpan dailyRewritingConfigTime;
        public static ThreadConfig Threadd = new ThreadConfig();
        public static ChannelPoolConfig ChannelPool;
        public static RMQConnection RmqConnection;
        public static List<SyncList> SyncList;
        public static IConfiguration SyncListConf { get; private set; }
        public static Serilog.Core.Logger Log = Logger.InitConfiguration(directory);

        public static string PerconaConnection;
        public static string BpmConnection;

        public static bool UseDatabaseAccess;
        private static string directory => Directory.GetCurrentDirectory();

        public static void InitConfig()
        {
            var conf = GetConf();
            InitDatabases(conf);

            #region RMQ connection
            RmqConnection = new RMQConnection
            {
                HostName = conf.GetSection("connectionStrings").GetSection("rabbitConnection")["hostName"],
                Port = Convert.ToInt32(conf.GetSection("connectionStrings").GetSection("rabbitConnection")["port"]),
                VirtualHost = conf.GetSection("connectionStrings").GetSection("rabbitConnection")["virtualHost"],
                UserName = conf.GetSection("connectionStrings").GetSection("rabbitConnection")["userName"],
                UserPassword = conf.GetSection("connectionStrings").GetSection("rabbitConnection")["userPassword"]
            };

            RmqConnection.BuildConnection();
            #endregion

            try
            {
                UseDatabaseAccess = Convert.ToBoolean(conf.GetSection("settings")["useDatabaseAccess"]);
                MaxSelectCount = Convert.ToInt32(conf.GetSection("settings")["maxSelectCount"]);
                LogPath = conf.GetSection("settings")["logPath"] ?? directory;
                ToSelectPrevDataSeconds = Convert.ToInt32(conf.GetSection("settings")["toSelectPrevDataSeconds"]);
                MaxInsertAttempts = Convert.ToInt32(conf.GetSection("settings")["maxInsertAttempts"]);
                dailyRewritingConfigTime = TimeSpan.Parse(conf.GetSection("settings")["dailyRewritingConfigTime"]);
                dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = "dd-MM-yyyy" };
                procedureName = conf.GetSection("settings")["procedureName"];
                isNeedConvertTypesFromRMQ = Convert.ToBoolean(conf.GetSection("settings")["isNeedConvertTypesFromRMQ"]);
                tableForLostOrders = conf.GetSection("settings")["tableForLostOrders"];
            }
            catch (Exception ex)
            {
                Log.Fatal($"System: one of parameters in appConfig.json is empty");
                throw ex;
            }

            JsonConvert.DefaultSettings = (() =>
            {
                var settings = new JsonSerializerSettings();
                settings.Converters.Add(new StringEnumConverter { CamelCaseText = true });
                return settings;
            });

            SyncList = BuildSyncConfig();

            InitChannelPoolConfig();

            Task.Run(() => InitScheduler());
        }

        private static IConfigurationRoot GetConf()
        {
            return new ConfigurationBuilder()
                .SetBasePath(directory)
                .AddJsonFile($"appConfig.json", optional: true, reloadOnChange: true)
                .Build();
        }

        private static void InitDatabases(IConfigurationRoot conf)
        {
            var dbs = conf.GetSection("connectionStrings").GetSection("databases");
            foreach (var item in dbs.GetChildren())
            {
                var alias = item.GetValue<string>("alias");
                var select = item.GetValue<string>("selectString");
                var insert = item.GetValue<string>("insertString");
                var add = item.GetValue<string>("addString");
                var type = item.GetValue<DbType>("type");
                DbConnector.Init(type, alias, select, insert, add);
            }
        }

        private static List<SyncList> BuildSyncConfig()
        {
            SyncListConf = new ConfigurationBuilder()
                .SetBasePath(directory)
                .AddJsonFile("synchronizationList.json", optional: true, reloadOnChange: true)
                .Build();

            var section = SyncListConf.GetSection("syncList");
            var res = new List<SyncList>();
            res.AddRange(GetFKeys(section.GetChildren()));
            return res;
        }

        public static List<SyncList> GetFKeys(IEnumerable<IConfigurationSection> syncItems)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
            var syncList = new List<SyncList>();

            foreach (var item in syncItems)
            {
                DateTime syncDate;

                if (item.GetValue<string>("SyncDate") == null || item.GetValue<string>("SyncDate") == "")
                {
                    syncDate = DateTime.ParseExact(DateTime.UtcNow.ToString("dd-MM-yyyy HH:mm:ss"), "dd-MM-yyyy HH:mm:ss", provider);
                }
                else
                {
                    syncDate = DateTime.ParseExact(item.GetValue<string>("SyncDate"), "dd-MM-yyyy", provider);
                }


                var syn = new SyncList
                {
                    Queue = item.GetValue<string>("Queue"),
                    SyncDate = syncDate,
                    SystemTarget = item.GetValue<DbType>("SystemTarget"),
                    ModelNameTarget = item.GetValue<string>("ModelNameTarget"),
                    SystemSource = item.GetValue<DbType>("SystemSource"),
                    IsNeedUpdate = item.GetValue<bool>("IsNeedUpdate"),
                    ModelNameSource = item.GetValue<string>("ModelNameSource"),
                    ChannelType = item.GetValue<ChannelType>("channelType"),
                    Exchange = item.GetValue<string>("Exchange"),
                    SyncTimeout = item.GetValue<int>("SyncTimeout"),
                    MaxInsertAttempts = item.GetValue<int>("MaxInsertAttempts"),
                    MapRules = new List<MapRules>(),
                    FK = new List<SyncList>()
                };

                syn.MapRules.AddRange(GetMapRules(item.GetSection("MapRules").GetChildren()));
                syn.FK.AddRange(GetFKeys(item.GetSection("FK").GetChildren()));

                syncList.Add(syn);
            }

            return syncList;
        }

        public static List<MapRules> GetMapRules(IEnumerable<IConfigurationSection> mapRules)
        {
            var mapRulesList = new List<MapRules>();
            foreach (var mr in mapRules)
            {
                var innerMapRule = new MapRules
                {
                    SourceColumnName = mr.GetValue<string>("SourceColumnName"),
                    TargetColumnName = mr.GetValue<string>("TargetColumnName"),
                    RuleType = mr.GetValue<RuleType>("RuleType"),
                    Value = mr.GetValue<string>("Value")
                };
                mapRulesList.Add(innerMapRule);
            }

            return mapRulesList;
        }

        public static bool CheckSettings()
        {
            var result = true;

            if (UseDatabaseAccess == true)
            {
                if (PerconaConnection == null || BpmConnection == null)
                {
                    Config.Log.Fatal($"System: one of connections in appConfig.json is empty");
                    result = false;
                }
            }
            return result;
        }

        private static void InitChannelPoolConfig()
        {
            ChannelPool = new ChannelPoolConfig();
        }

        public static void UpdateSyncDate(string queue)
        {
            var cur = SyncList.Where(sl => sl.Queue == queue).FirstOrDefault();
            cur.SyncDate = DateTime.Today;
        }

        public static void UpdateConfig()
        {
            try
            {
                Log.Information("Rewriting config...");
                var section = new JObject();

                section["syncList"] = JToken.FromObject(SyncList);

                string output = JsonConvert.SerializeObject(section, Newtonsoft.Json.Formatting.Indented, dateTimeConverter);
                File.WriteAllText("synchronizationList.json", output);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }

        public static async Task InitScheduler()
        {
            var props = new NameValueCollection {
                 { "quartz.serializer.type", "binary" }
            };

            var factory = new StdSchedulerFactory(props);

            IScheduler sched = await factory.GetScheduler();
            await sched.Start();

            IJobDetail job = JobBuilder.Create<UpdateJob>()
                .WithIdentity("myJob", "group1")
                .Build();

            ITrigger trigger = TriggerBuilder.Create()
                                            .WithIdentity("myTrigger", "group1")
                                            .StartNow()
                                            .WithSchedule(CronScheduleBuilder.
                                                DailyAtHourAndMinute(dailyRewritingConfigTime.Hours,
                                                dailyRewritingConfigTime.Minutes))
                                            .Build();

            await sched.ScheduleJob(job, trigger);
        }
    }

    public class UpdateJob : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            await Task.Run(() => Config.UpdateConfig());
        }
    }

    public class ChannelConfig
    {
        public string Queue;
        public bool Durable;
        public bool Exclusive;
        public bool AutoDelete;
        public string Exchange;
        public IDictionary<string, object> Arguments;
        public DbType Target;
        public DbType Source;
        public int SyncTimeout;
        internal int SleepTime;
    }

    public class ThreadConfig : IConfig
    {
        public int ProcessorCount;

        public ThreadConfig()
        {
            InitConfig();
        }

        public void InitConfig()
        {
            ProcessorCount = Environment.ProcessorCount;
        }
    }

    public class ChannelPoolConfig
    {
        public int MaxPoolCount;
        public int MaxChannelCount;

        public ChannelPoolConfig()
        {
            CalculateMaxPoolCount();
            CalculateMaxChannelCount();
        }

        private void CalculateMaxPoolCount()
        {
            if (Config.SyncList.Count == 1)
                MaxPoolCount = 1;
            else
                MaxPoolCount = Config.Threadd.ProcessorCount / 2;
        }

        private void CalculateMaxChannelCount()
        {
            float count = (float)Config.SyncList.Where(x => x.SystemTarget == DbType.MSSQL).Count() / (float)MaxPoolCount;
            MaxChannelCount = (int)Math.Ceiling(count);
        }
    }

    public class SyncList
    {
        public string Queue;
        public bool IsNeedUpdate;
        public DateTime SyncDate;
        public DbType SystemTarget;
        public string ModelNameTarget;
        public DbType SystemSource;
        public string ModelNameSource;
        public string Exchange;
        public int SyncTimeout;
        public int MaxInsertAttempts;
        public ChannelType ChannelType;
        public List<MapRules> MapRules;
        public List<SyncList> FK;

    }

    public class MapRules
    {
        public string SourceColumnName;
        public string TargetColumnName;
        public RuleType RuleType;
        public string Value;
    }

    public class RMQConnection
    {
        public string HostName;
        public int Port;
        public string UserName;
        public string UserPassword;
        public string VirtualHost;

        private IConnection _connectionFactory;

        public void BuildConnection()
        {
            _connectionFactory = new ConnectionFactory()
            {
                HostName = HostName,
                Port = Port,
                UserName = UserName,
                Password = UserPassword,
                VirtualHost = VirtualHost,
                AutomaticRecoveryEnabled = true,
            }
            .CreateConnection();

            _connectionFactory.AutoClose = false;
        }

        public IConnection GetConnection()
        {
            return _connectionFactory;
        }
    }

    public enum RuleType
    {
        Simple = 1,
        Multiplication,
        DefaultValue,
        Ignore,
        AddHours,
        BAToGuid,
        GuidToBA,
        Sql,
        Default,
        TimeZone,
        SimpleDate /*r.troian*/
    }

    public enum ChannelType
    {
        DbChannel = 1,
        RmqChannel = 2
    }
}
