﻿using RabbitMQ.Client;

namespace Consimple.Workers
{
    public static class RmqExtensions
    {
        public static QueueDeclareOk QueueDeclare(this IModel channel, ChannelConfig config)
        {
            return channel.QueueDeclare
            (
                queue: config.Queue,
                durable: config.Durable,
                exclusive: config.Exclusive,
                autoDelete: config.AutoDelete,
                arguments: config.Arguments
            );
        }
    }
}
