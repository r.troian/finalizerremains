﻿using System.Collections.Generic;
using System.Linq;

namespace Consimple.Workers
{
    public static class CollectionExtensions
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> list)
        {
            if (list == null || list.Count() == 0)
                return true;

            return false;
        }
    }
}
