﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;

namespace Consimple.Workers
{
    public class WorkerSync
    {
       
        
        public List<IChannelPool> WorkerChannelList;
        public WorkerSync()
        {
            InitConfig();
            InitEvents();
            WorkerChannelList = BildChannelPool();
        }

        public IChannelPool GetWorkerChannelByQueueName(string queueName)
        {
            return WorkerChannelList.Find(x => x.Name == queueName);
        }

        public IChannelPool GetWorkerChannelByQueueName(string queueName, DateTime syncDate)
        {
            var res = WorkerChannelList.Find(x => x.Name == queueName);
            ((ChannelBase)res).sync.SyncDate = syncDate;
            return res;
        }

        private List<IChannelPool> BildChannelPool()
        {
            var res = new List<IChannelPool>();

            foreach (var sync in Config.SyncList)
            {
                IChannelPool channel;

                if (sync.ChannelType == ChannelType.RmqChannel)
                    channel = new RMQChannel(sync);
                else
                    channel = new DbAutoChannel(sync);

                channel.InitLogger(Config.LogPath);
                res.Add(channel);
            }

            if (res.ChackNotDistinctName())
            {
                throw new Exception($"TableList mast contain only unic QueueName: [{string.Join(", ", res.GetNotDistinctName())}]!");
            }
            return res;
        }




        private static void InitConfig()
        {
            Thread.CurrentThread.Name = "Main";
            Config.Log.Information("Application starting");
            Config.InitConfig();
            Config.Log.Information("Check settings");

            //if (!Config.CheckSettings())
            //{
            //    Config.Log.Warning("System: application will stop after 10 seconds...");
            //    Thread.Sleep(10000);
            //    Environment.Exit(0);
            //}
        }

        private static void InitEvents()
        {
            _handler += new EventHandler(Handler);
            SetConsoleCtrlHandler(_handler, true);
        }

      

        #region Close event  
        private static EventHandler _handler;

        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);

        private static bool Handler(CtrlType sig)
        {
            Config.UpdateConfig();

            Environment.Exit(-1);

            return true;
        }

        private delegate bool EventHandler(CtrlType sig);

        private enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }
        #endregion
    }
}
