﻿using System;
using System.Collections.Generic;
using System.Data;
using Consimple.Workers.Db;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Consimple.Workers
{
    public abstract class ChannelBase : IChannel, IChannelPool
    {
        #region fields
        protected string getListQuery;
        public SyncList sync;
        protected DbType target;
        protected DbType source;
        protected ModelBuilder modelBuilder;
        protected Serilog.Core.Logger logger;
        protected IMapper mapper;
        #endregion
        public abstract dynamic Map(ModelBuilder model, dynamic row);

        public string Name { get; set; }

        public abstract void Listen(IConnection connection = null);

        public ChannelBase(SyncList sync)
        {
            SetSync(sync);
            modelBuilder = new ModelBuilder(sync);
            target = sync.SystemTarget;
            source = sync.SystemSource;
            mapper = new MapperBase(sync.ChannelType, sync.SystemTarget);
        }

        //public void InitLogger()
        //{
        //    logger = Logger.InitConfiguration(sync.ModelNameTarget);
        //}
        public void InitLogger(string directory)
        {
            logger = Logger.InitConfiguration(sync.ModelNameTarget, directory);
        }

        #region starting
        protected void BuildModels()
        {
            modelBuilder.BuildModels();
            getListQuery =  modelBuilder.GetSourceListQuery();
        }

        protected void SetSync(SyncList sync)
        {
            this.sync = sync ?? throw new NullReferenceException($"Value cannot be 'null'. Source: DbAccessedChannel.SetSyncData(SyncData sync)");
            Name = sync.Queue;
        }
        #endregion

        protected void UpdateInsert(IDbConnection db, dynamic item, ModelBuilder model)
        {
            if (db.State == ConnectionState.Closed)
                db.Open();

            var rowsAffected = 0;
            rowsAffected = Update(db, item, model);

            if (rowsAffected < 1)
                Insert(db, item, model);

        }

        protected int Update(IDbConnection db, dynamic item, ModelBuilder model)
        {
            string query = model.GetTargetUpdateStatement();
            return Dapper.SqlMapper.Execute(db, sql: query, param: item, commandTimeout: 0);
        }

        protected int Insert(IDbConnection db, dynamic item, ModelBuilder model)
        {
            string query = model.GetTargetInsertStatement();
            return Dapper.SqlMapper.Execute(db, sql: query, param: item, commandTimeout: 0);
        }

        public abstract Dictionary<string, object> Apply();
      
    }
}
