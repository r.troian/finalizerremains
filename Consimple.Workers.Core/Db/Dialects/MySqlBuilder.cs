﻿namespace Consimple.Workers.Db
{
    class MySqlBuilder : QueryBuilderBase
    {
        public override string GetQuotedColumn(string columnName)
        {
            return $"`{columnName}`";
        }

        public override string GetQuotedTable(string tableName)
        {
            return $"`{tableName}`";
        }
    }
}
