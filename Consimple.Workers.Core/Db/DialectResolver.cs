﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using Consimple.Workers.Interfaces;

namespace Consimple.Workers.Db
{
    public static class DialectResolver
    {
        private static readonly Dictionary<Type, Func<IDialect>> _dict = new Dictionary<Type, Func<IDialect>> {
            { typeof(SqlConnection), () => new SqlBuilder() },
            { typeof(MySqlConnection), () => new MySqlBuilder() }};

        internal static IDialect GetDialect<T>()
            where T : IDbConnection
        {
            return GetDialect(typeof(T));
        }

        internal static IDialect GetDialect(this IDbConnection db)
        {
            return GetDialect(db.GetType());
        }

        internal static IDialect GetDialect(this IDbConnection db, DbType target)
        {
            if(target == DbType.MSSQL)
                return GetDialect(typeof(SqlConnection));
            if (target == DbType.MYSQL)
                return GetDialect(typeof(MySqlConnection));
           // if (target == DbType.POSTEGRESSQL) 
              else  return new PostgersSqlBuilder();
        }


        private static IDialect GetDialect(Type t)
        {
            if (_dict.TryGetValue(t, out Func<IDialect> value))
            {
                return value();
            }
            return null;
        }
    }
}
