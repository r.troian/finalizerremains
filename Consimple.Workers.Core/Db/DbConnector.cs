﻿using MySql.Data.MySqlClient;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Consimple.Workers
{
    public class DbConnector
    {
        private static Dictionary<string, DbInfo> _databases = new Dictionary<string, DbInfo>();

        public static void Init(DbType type, string alias, string selectString, string insertString, string addString)
        {
            _databases.TryAdd(alias, new DbInfo(selectString, insertString, addString, type));
        }

        public static IDbConnection GetConnection(string alias, ConnectionStringType stringType = ConnectionStringType.Select)
        {
            if (_databases.TryGetValue(alias, out DbInfo db))
            {
                return ResolveConnection(db, stringType);
            }
            return null;
        }

        public static IDbConnection GetConnection(DbType dbType, ConnectionStringType stringType = ConnectionStringType.Select)
        {
            var item = _databases.FirstOrDefault(x => x.Value.DbType == dbType);
            return ResolveConnection(item.Value, stringType);
        }

        private static IDbConnection ResolveConnection(DbInfo db, ConnectionStringType stringType)
        {
            if (db.DbType == DbType.MSSQL)
                return new SqlConnection(db.GetConnectionString(stringType));

            if (db.DbType == DbType.MYSQL)
                return new MySqlConnection(db.GetConnectionString(stringType));

            if (db.DbType == DbType.POSTEGRESSQL)
                return new NpgsqlConnection(db.GetConnectionString(stringType));

            return null;
        }
    }

    public class DbInfo
    {
        public DbInfo(string selectConnString, string insertConnSelect, string addString, DbType type)
        {
            selectString = selectConnString;
            insertString = insertConnSelect;
            this.addString = addString;
            DbType = type;
        }

        public DbType DbType;

        private string selectString;
        private string insertString;
        private string addString;

        public string GetConnectionString(ConnectionStringType type)
        {
            switch (type)
            {
                case ConnectionStringType.Select:
                    return selectString;
                case ConnectionStringType.Insert:
                    return insertString;
                case ConnectionStringType.Else:
                    return addString;
                default:
                    return selectString;
            }
        }

        public string GetConnectionString(int type)
        {
            return GetConnectionString((ConnectionStringType)type);
        }
    }

    public enum ConnectionStringType
    {
        Select,
        Insert,
        Else
    }
}
