﻿using Consimple.Workers.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace Consimple.Workers.Db
{
    public class ModelBuilder
    {
        public static object locker = new object();
        #region properties  
        public SyncList config;
        public List<string> sourceColumns;
        public List<string> targetColumns;
        public List<KeyValuePair<string, Type>> sourceColumnsBase;
        public DbConnector connector;
        public string NameSource { get; set; }
        public string NameTarget { get; set; }
        public List<ModelBuilder> SubModels;
        public IDialect SourceBuilder { get; set; }
        public IDialect TargetBuilder { get; set; }

        public DbType target;
        public DbType source;
        #endregion

        public ModelBuilder() { }

        public ModelBuilder(SyncList config)
        {
            NameSource = config.ModelNameSource;
            NameTarget = config.ModelNameTarget;
            this.config = config;
            target = (DbType)config.SystemTarget;
            source = (DbType)config.SystemSource;
            connector = new DbConnector();
            this.SubModels = new List<ModelBuilder>();
            foreach (var mm in config.FK)
            {
                var model = new ModelBuilder();
                model.NameSource = mm.ModelNameSource;
                model.NameTarget = mm.ModelNameTarget;
                model.config = mm;
                model.target = (DbType)config.SystemTarget;
                model.source = (DbType)config.SystemSource;
                this.SubModels.Add(model);
            }
        }

        public void BuildModels()
        {
            BuildSource();
            BuildTarget();
            RemoveExcessColumns();
            foreach (var sm in this.SubModels)
            {
                sm.RemoveExcessColumns();
            }
            var getList = SourceBuilder.GetListFromSource(config.ModelNameSource, sourceColumns);

            CreateStructureFile();
        }

        public void CreateStructureFile()
        {
            lock (locker)
            {
                using (StreamWriter sw = new StreamWriter(@"SyncListStructure.txt", true, System.Text.Encoding.Default))
                {
                    sw.WriteLine("Queue Name: " + config.Queue);
                    sw.WriteLine("Source: " + NameSource);
                    sw.WriteLine("Target: " + NameTarget);

                    WriteModelColumns(this, sw);
                    sw.WriteLine("   ");
                    sw.WriteLine("-------------------------FK--------------------------------");
                    foreach (var model in SubModels)
                    {
                        sw.WriteLine("FK Name: " + model.config.Queue);
                        sw.WriteLine("Source: " + model.NameSource);
                        sw.WriteLine("Target: " + model.NameTarget);
                        WriteModelColumns(model, sw);
                        sw.WriteLine("   ");
                    }
                    sw.WriteLine("   ");
                    sw.WriteLine("   ");
                    sw.WriteLine("-----------------------END---------------------------");
                    sw.WriteLine("   ");
                    sw.WriteLine("   ");
                }
            }
        }

        public void WriteModelColumns(ModelBuilder model, StreamWriter sw)
        {
            foreach (var sc in model.sourceColumns)
            {
                var target = model.targetColumns.Where(tc => tc == sc).FirstOrDefault();

                if (target is null)
                    target = model.config.MapRules.Where(mp => mp.SourceColumnName == sc).FirstOrDefault().TargetColumnName;
                sw.WriteLine(sc + "-->" + target);
            }
        }

        public string GetSourceListQuery()
        {
            return SourceBuilder.GetListFromSource(config.ModelNameSource, sourceColumns);
        }
        public virtual string GetTargetUpdateStatement()
        {
            return TargetBuilder.GetUpdateStatement(this.NameTarget, this.targetColumns);
        }
        public virtual string GetTargetInsertStatement()
        {
            return TargetBuilder.GetInsertStatement(this.NameTarget, this.targetColumns);
        }

        protected void BuildSource()
        {
            using (var source = DbConnector.GetConnection(this.source))
            {
                SourceBuilder = source.GetDialect(this.source);
                sourceColumns = BuildModelSource(source, SourceBuilder, this.NameSource, out sourceColumnsBase);

                foreach (var n in this.SubModels)
                {
                    n.SourceBuilder = this.SourceBuilder;
                    n.sourceColumns = BuildModelSource(source, SourceBuilder, n.NameSource,  out n.sourceColumnsBase);
                }
            }
        }
        protected void BuildTarget()
        {
            using (var target = DbConnector.GetConnection(this.target))
            {
                TargetBuilder = target.GetDialect(this.target);
                targetColumns = BuildModelTarget(target, TargetBuilder, this.NameTarget);

                foreach (var n in this.SubModels)
                {
                    n.TargetBuilder = this.TargetBuilder;
                    n.targetColumns = BuildModelTarget(target, TargetBuilder, n.NameTarget);
                }
            }
        }

        protected List<string> BuildModelSource(IDbConnection db, IDialect builder, string modelName, 
            out List<KeyValuePair<string, Type>> sourceColumnsBase )
        {

            var command = db.CreateCommand();
            command.CommandText = builder.SelectTopAllFrom(modelName);

            if(db.State == ConnectionState.Closed)
             db.Open();
            sourceColumnsBase = new List<KeyValuePair<string, Type>>();
            var columns = new List<string>();
            using (var reader = command.ExecuteReader())
            {
                var fieldCount = reader.FieldCount;
                for (int i = 0; i < fieldCount; i++)
                {
                    columns.Add(reader.GetName(i));
                    sourceColumnsBase.Add(new KeyValuePair<string, Type>(reader.GetName(i), reader.GetFieldType(i)));
                }
            }
            return columns;
        }
        protected List<string> BuildModelTarget(IDbConnection db, IDialect builder, string modelName)
        {
            var command = db.CreateCommand();
            command.CommandText = builder.SelectTopAllFrom(modelName);

            if (db.State == ConnectionState.Closed)
                db.Open();
   
            var columns = new List<string>();
            using (var reader = command.ExecuteReader())
            {
                var fieldCount = reader.FieldCount;
                for (int i = 0; i < fieldCount; i++)
                {
                    columns.Add(reader.GetName(i));
                }
                // return GetColumns(reader);
            }
            return columns;
        }

        protected List<string> GetColumns(IDataReader reader)
        {
            var fieldCount = reader.FieldCount;
            var columns = new List<string>();
            for (int i = 0; i < fieldCount; i++)
            {
                columns.Add(reader.GetName(i));
                sourceColumnsBase.Add(new KeyValuePair<string, Type>(reader.GetName(i), reader.GetFieldType(i)));
            }
            return columns;
        }
        protected void RemoveExcessColumns()
        {
            this.sourceColumns = GetNecessarySourceColumns().ToList();
            this.targetColumns = GetNecessaryTagetColumns().ToList();
        }
        protected IEnumerable<string> GetNecessarySourceColumns()
        {
            return sourceColumns
                .Where(sc => IsNecessarySourceColumn(sc));
        }
        protected IEnumerable<string> GetNecessaryTagetColumns()
        {
            return targetColumns
                .Where(tc => IsNecessaryTargetColumn(tc));
        }
        protected bool IsNecessarySourceColumn(string columnName)
        {
            var res = targetColumns.Contains(columnName);
            if (config.MapRules != null && config.MapRules.Count > 0)
                return res ||
                   config.MapRules.Exists(mr => mr.SourceColumnName == columnName);
            return res;
        }
        protected bool IsNecessaryTargetColumn(string columnName)
        {
            var res = sourceColumns.Contains(columnName);
            if (config.MapRules != null && config.MapRules.Count > 0)
                return res || config.MapRules.Exists(mr => mr.TargetColumnName == columnName);
            return res;
        }
    }

}
