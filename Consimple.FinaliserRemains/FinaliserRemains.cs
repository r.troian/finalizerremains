﻿using System;
using System.Collections.Generic;

namespace Consimple.FinaliserRemains
{
    public class FinaliserRemains
    {
        public Config config;
        public List<Sequence> SequenceList;

        public FinaliserRemains()
        {
            config = new Config();
            SequenceList = config.GetSequenceList("SequenceList.json");
        }
        public FinaliserRemains(string configFileName)
        {
            config = new Config();
            SequenceList = config.GetSequenceList(configFileName);
        }
    }
}
