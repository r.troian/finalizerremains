﻿using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.IO;
using System;


namespace Consimple.FinaliserRemains
{
    public class  Config : ConfigBase
    {
        public int CountSkipStep;

        public Config(string configFileName = "appConfig.json", string directoryPath = null)
        {
            directory = directoryPath?? Directory.GetCurrentDirectory();
            Init(configFileName);
        }


        protected override void InitApp(IConfigurationRoot conf)
        {
            LogPath = conf.GetSection("settings")["loggerPath"] ?? directory;
            CountSkipStep = conf.GetSection("settings")["CountSkipStep"] != null || conf.GetSection("settings")["CountSkipStep"] != "" ? conf.GetSection("settings").GetValue<int>("CountSkipStep") : 0;
        }

        public List<Sequence> GetSequenceList( string configFileName, string _directory = null)
        {
            var conf = GetConf(configFileName, _directory);
            return GetSequenceListByConfig(conf);
        }

        protected List<Sequence> GetSequenceListByConfig(IConfigurationRoot conf)
        {
            var sequenceList = new List<Sequence>();
            var section = conf.GetSection("SequenceList");
            var parseList = section.GetChildren();
            foreach (var item in parseList)
            {
                var sectionChild = new Sequence
                {
                    Name = item.GetValue<string>("Name"),
                    QueueType = item.GetValue<QueueType>("QueueType"),
                    QueueName = item.GetValue<string>("QueueName"),
                    SleepTimeout = item["SleepTimeout"] == null ? 0 : item.GetValue<int>("SleepTimeout")
                };
                sequenceList.Add(sectionChild);
            }
            return sequenceList;
        }
    }


    public class Sequence
    {
        public string Name;
        public QueueType QueueType;
        public string QueueName;
        public int SleepTimeout;
    }

    public enum QueueType
    {
        WorkerSync,
        UpdateModifiedOn,
        UnwantedSql
    }
}
