﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Consimple
{
    public static class DictionaryExtension
    {
        public static void AddRange(this Dictionary<string, object> dic, Dictionary<string, object> items)
        {
            foreach (var item in items)
            {
                dic.Add(item.Key, item.Value);
            }
        }   
    }


    public static class ChanelPoolExtension
    {
        public static bool ChackNotDistinctName(this List<IChannelPool> pools)
        {
            return pools.GroupBy(v => v.Name).Where(g => g.Count() > 1).Select(g => g.Key).Count() > 0;
        }

        public static List<string> GetNotDistinctName(this List<IChannelPool> pools)
        {
            return pools.GroupBy(v => v.Name).Where(g => g.Count() > 1).Select(g => g.Key).ToList();
        }
    }




}
