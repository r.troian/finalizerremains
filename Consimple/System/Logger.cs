﻿using System.Threading;

using Serilog;
using Serilog.Events;
using Serilog.Core;


namespace Consimple
{
    public static class Logger 
    {
        private static object obj = new object();
        public static Serilog.Core.Logger InitConfiguration(string directory, bool console = false)
        {
            return InitConfiguration("system", directory, console);
        }
  
        public static Serilog.Core.Logger InitConfiguration(string fileName, string directory, bool console = false)
        {
            Serilog.Core.Logger log;

            if (console)
            {
                log = new LoggerConfiguration()
               .Enrich.With(new ThreadIdEnricher())
               .MinimumLevel.Debug()
               .WriteTo.ColoredConsole(restrictedToMinimumLevel: LogEventLevel.Information)
               .WriteTo.File($"{directory}\\Logs\\{fileName}-log-.txt", rollingInterval: RollingInterval.Day,
                   outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}")
               .CreateLogger();
            }
            else
            {
                log = new LoggerConfiguration()
               .Enrich.With(new ThreadIdEnricher())
               .MinimumLevel.Debug()
               //.WriteTo.ColoredConsole(restrictedToMinimumLevel: LogEventLevel.Information)
               .WriteTo.File($"{directory}\\Logs\\{fileName}-log-.txt", rollingInterval: RollingInterval.Day,
                   outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}")
               .CreateLogger();
            }
           
            return log;
        }

        public static void LogError(this Serilog.Core.Logger logger, string queue, string message)
        {
            lock (obj)
            {
                logger.Error($"=> SyncBpmProcessing_{ queue }: {message}");
            }
        }

        public static void LogInfo(this Serilog.Core.Logger logger, string queue, string message)
        {
            lock (obj)
            {
                logger.Information($"=> SyncBpmProcessing_{ queue }: {message}");
            }
        }
    }



    public class ThreadIdEnricher : ILogEventEnricher
    {
        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            logEvent.AddPropertyIfAbsent(propertyFactory.CreateProperty(
                    "ThreadId", Thread.CurrentThread.ManagedThreadId));
        }
    }
}
