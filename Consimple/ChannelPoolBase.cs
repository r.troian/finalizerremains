﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Consimple
{
    public abstract class ChannelPoolBase : IChannelPool
    {
        public string Name { get; set; }
        protected Serilog.Core.Logger logger;

        public void InitLogger(string directoryPathForLogger)
        {
            logger =  Logger.InitConfiguration(Name, directoryPathForLogger);
            logger.Information($"----- Init Logger [{Name}] ----\n");
        }

        public abstract Dictionary<string, object> Apply();
       
    }
}
