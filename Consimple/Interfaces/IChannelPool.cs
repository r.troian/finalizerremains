﻿using System;
using System.Collections.Generic;
using System.Text;



namespace Consimple
{
    public interface IChannelPool
    {
        string Name { get; set; }
        void InitLogger(string directory);
        Dictionary<string, object> Apply();

    }
}
