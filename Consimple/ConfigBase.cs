﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace Consimple
{
    public abstract  class ConfigBase
    {
        public string directory;
        public string LogPath;

        public virtual IConfigurationRoot GetConf(string configFileName, string _directory = null)
        {
            IConfigurationRoot conf;
            _directory = _directory ?? directory;

            try
            {
                if (!File.Exists($"{_directory}\\{configFileName}"))
                {
                    throw new Exception($"Config file is not exist or cannot open it!");
                }

                conf = new ConfigurationBuilder()
               .SetBasePath(_directory)
               .AddJsonFile(configFileName, optional: true, reloadOnChange: true)
               .Build();
            }
            catch (Exception ex)
            {
                throw new Exception($"Error ConfigFile:[{_directory}\\{configFileName}] \nMessage:[{ex.Message}]\nStackTrace:[{ex.StackTrace}]");
            }

            return conf;
        }

        

        public virtual void Init(string configFileName)
        {           
            var conf = GetConf(configFileName);
            InitApp(conf);
        }

        protected abstract void InitApp(IConfigurationRoot conf);

    }
}
