﻿using System;
using System.Collections.Generic;


using System.Linq;

using System.IO;

namespace Consimple.UpdateModifiedOn
{
    public class UpdChannelPool : ChannelPoolBase
    {
        public string constMsConnectinString;
        public string constMyConnectinString;
        public int countErrorTry;
        public bool saveToFile;
        public int packageSize;
        public string LogPath;

        public string PerconaTable { get; set; }
        public string BpmTable { get; set; }
        public string ExtendedBpmColumnName { get; set; }
        public string ExtendedPerconaColumnName { get; set; }
        public Action Action { get; set; }
        public SyncDirect SyncDirect { get; set; }

        UnwantedSql.MySqlExec perconaProvider;
        UnwantedSql.MsSqlExec msProvider;
        private Table _table;



        public UpdChannelPool(Table table, Config config)
        {
            _table = table;
            Name = table.Queue;
            LogPath = config.LogPath;
            countErrorTry = config.countErrorTry;
            saveToFile = config.saveToFile;
            packageSize = config.packageSize;
            constMsConnectinString = config.constMsConnectinString;
            constMyConnectinString = config.constMyConnectinString;

            PerconaTable = table.PerconaTable;
            BpmTable = table.BpmTable;
            ExtendedBpmColumnName = table.ExtendedBpmColumnName;
            ExtendedPerconaColumnName = table.ExtendedPerconaColumnName;
            Action = table.Action;
            SyncDirect = table.SyncDirect;

            InitLogger(config.LogPath);
            try
            {
                perconaProvider = new UnwantedSql.MySqlExec(constMyConnectinString);
                msProvider = new UnwantedSql.MsSqlExec(constMsConnectinString);
            }
            catch (Exception ex)
            {
                logger.Fatal($"Uncorrect ConnectionString Err: message[{ex.Message}]");
                throw ex;
            }

        }

        public override Dictionary<string, object> Apply()
        {
            var res = new Dictionary<string, object>();
            string objName = $"UpdateModifiedOn - Queue: [{Name}], SyncDirect: [{SyncDirect}], Action: [{Action}]";            
            res.Add(objName, "-->");
            try
            {
                res.AddRange(ApplyListen(logger, _table, msProvider, perconaProvider, packageSize, countErrorTry, LogPath, saveToFile));
            }
            catch(Exception ex)
            {
                throw new UnwantedSql.SqlException($"Error UpdateModifiedOn  Message:[{ex.Message}]\n StackTrace:[{ex.StackTrace}]");
            }
           
            
            return res;
        }

        private static Dictionary<string, object> ApplyListen(Serilog.Core.Logger logger, Table item, UnwantedSql.MsSqlExec msProvider, UnwantedSql.MySqlExec perconaProvider, int packageSize, int countErrorTry, string LogPath,  bool saveToFile = false) 
        {         
            var res = new Dictionary<string, object>();

            logger.Information($"SyncDirect - {item.SyncDirect}"); 
            logger.Information($"Action - {item.Action}");
            IEnumerable<DataModel> msData = null;
            IEnumerable<DataModel> myData = null;
            IEnumerable<DataModel> missingDataInBpm = null;
            IEnumerable<DataModel> missingDataInPercona = null;

            IEnumerable<Guid> missingDataInBpm_Id = new List<Guid>();
            IEnumerable<Guid> missingDataInPercona_Id = new List<Guid>();

            res.Add("Tables BPM/Percona", $"{item.BpmTable}[{item.ExtendedBpmColumnName}]/{item.PerconaTable}[{item.ExtendedBpmColumnName}]");
            string extendedBpmColumnName = string.IsNullOrEmpty(item.ExtendedBpmColumnName) ? "NULL" : item.ExtendedBpmColumnName;
            string extendedPerconaColumnName = string.IsNullOrEmpty(item.ExtendedPerconaColumnName) ? "NULL" : item.ExtendedPerconaColumnName;
            logger.Information($"ExtendedColumnName - BPM.{item.BpmTable}[{extendedBpmColumnName}]");
            logger.Information($"ExtendedColumnName - Percona.{item.PerconaTable}[{extendedPerconaColumnName}]");
           

            if (item.SyncDirect == SyncDirect.bpmToPercona)
            {
                msData = msProvider.Query<DataModel>(
                    $"SELECT Id as Id, {extendedBpmColumnName} as ExtendedColumn FROM [{item.BpmTable}]",commandTimeout:0);

                logger.Information($"BPM table - '{item.BpmTable}' count = [{msData.Count()}]");

                myData = perconaProvider.Query<DataModel>(
                      $"SELECT Id as Id, {extendedPerconaColumnName} as ExtendedColumn FROM {item.PerconaTable}");
                logger.Information($"Percona table - '{item.PerconaTable}' count = [{ myData.Count()}]");
            }
            else
            {
                myData = perconaProvider.Query<DataModel>(
                    $"SELECT Id as Id, {extendedPerconaColumnName} as ExtendedColumn FROM {item.PerconaTable}");
                logger.Information($"Percona table - '{item.PerconaTable}' count = [{ myData.Count()}]");

                msData = msProvider.Query<DataModel>(
                    $"SELECT Id, {extendedBpmColumnName} as ExtendedColumn FROM [{item.BpmTable}]");
                logger.Information($"BPM table - '{item.BpmTable}' count = [{msData.Count()}]");
            }


          //if ((Action != Action.analysisPercona && Action != Action.update) || SyncDirect != SyncDirect.bpmToPercona)
            if (item.SyncDirect == SyncDirect.perconaToBpm || item.SyncDirect == SyncDirect.duplex)
            {
                missingDataInBpm = Compare(myData, msData);
                missingDataInBpm_Id = GetIdFromDataModel(missingDataInBpm);
                res.Add("Detected missing data in BPM", $"{missingDataInBpm.Count()}");
                logger.Information($"Detected missing data in BPM - [{missingDataInBpm.Count()}]");

                if (item.Action == Action.update)
                {
                    int resUpdatePercona = Update(missingDataInBpm_Id, perconaProvider, item.PerconaTable, logger, packageSize, countErrorTry);
                    logger.Information($"Update row in [PerconaTables-'{item.PerconaTable}'] - [{resUpdatePercona}]");
                    res.Add("Update in Percona", $"{resUpdatePercona}");
                }
                else if (item.Action == Action.delete)
                {
                    int resUpdatePercona = Delete(missingDataInBpm_Id, perconaProvider, item.PerconaTable, logger, packageSize, countErrorTry);
                    logger.Information($"Delete row in [PerconaTables-'{item.PerconaTable}'] - [{resUpdatePercona}]");
                    res.Add("Delete in Percona", $"{resUpdatePercona}");              
                }
            }

          //if ((Action != Action.analysisBpm && Action != Action.update) || SyncDirect != SyncDirect.perconaToBpm)
            if (item.SyncDirect == SyncDirect.bpmToPercona || item.SyncDirect == SyncDirect.duplex)
            {
                missingDataInPercona = Compare(msData, myData);
                missingDataInPercona_Id = GetIdFromDataModel(missingDataInPercona);
                res.Add("Detected missing data in Percona", $"{missingDataInPercona.Count()}");
                logger.Information($"Detected missing data in Percona - [{missingDataInPercona.Count()}]");

                if (item.Action == Action.update)
                {
                    int resUpdateBpm = Update(missingDataInPercona_Id, msProvider, item.BpmTable, logger, packageSize, countErrorTry);
                    logger.Information($"Update row in [BpmTables-'{item.BpmTable}'] - [{resUpdateBpm}]");
                    res.Add("Update in BPM", $"{resUpdateBpm}");
                }
                else if (item.Action == Action.delete)
                {
                    int resUpdateBpm = Delete(missingDataInPercona_Id, msProvider, item.BpmTable, logger, packageSize, countErrorTry);
                    logger.Information($"Delete row in [BpmTables-'{item.BpmTable}'] - [{resUpdateBpm}]");
                    res.Add("Delete in BPM", $"{resUpdateBpm}");
                }

            }

            if (saveToFile)
            {
                res.Add("SevingToFile", $"True");
                ToFile(missingDataInBpm_Id, "Percona", item.PerconaTable, item.ExtendedPerconaColumnName, logger, LogPath);
                ToFile(missingDataInPercona_Id, "BPM", item.BpmTable, item.ExtendedBpmColumnName, logger, LogPath);
            }

            msData = null;
            myData = null;
            missingDataInBpm = null;
            missingDataInPercona = null;
            missingDataInBpm_Id = null;
            missingDataInPercona_Id = null;

            return res;
        }


        private static  IEnumerable<Guid> GetIdFromDataModel(IEnumerable<DataModel> md)
        {
            var res = new List<Guid>();

            foreach (var it in md)
            {
                res.Add(
                    (Guid)it.Id
                );
            }

            return res;
        }

        private static async void ToFile(IEnumerable<Guid> Ids, string systemName, string tatableName, string extendedColumnName, Serilog.Core.Logger logger, string LogPath)
        {

            if (Ids == null || Ids.Count() == 0) { return; }

            extendedColumnName = string.IsNullOrEmpty(extendedColumnName) ? "" : $"[{extendedColumnName}]_";
            logger.Information($"Saving '{tatableName}_{systemName}_{extendedColumnName}Ids'");

            string writePath = $@"{LogPath}\\{tatableName}_{systemName}_{extendedColumnName}Ids.txt";

            try
            {
                using (StreamWriter sw = File.CreateText(writePath))// new StreamWriter(writePath, false, System.Text.Encoding.Default))
                {
                    await sw.WriteLineAsync($"{DateTime.Now}  | UTC - {DateTime.UtcNow}  | Count - {Ids.Count()}");

                    foreach (var item in Ids)
                    {
                        if (systemName == "Percona")
                        {
                            await sw.WriteLineAsync($" UuidToBin('{item}'),");
                        }
                        else
                        {
                            await sw.WriteLineAsync($" '{item}',");
                        }

                    }
                }

                logger.Information($"Saved to '{writePath}'");
            }
            catch (Exception e)
            {
                logger.Error($"Invalid save to {writePath} Err: message [{e.Message}]");
            }

        }

        private static IEnumerable<T> Compare<T>(IEnumerable<T> first, IEnumerable<T> second)
        {
            return first.Except(second);
        }

        private static int Update(IEnumerable<Guid> Ids, UnwantedSql.DbProviderBase BbProvider, string table, Serilog.Core.Logger logger, int packageSize, int countErrorTry)
        {
            int result = 0;
            int counter = 0;
            int pack = packageSize;

            if (Ids == null) { return -1; }


            while (counter < Ids.Count())
            {
                if (counter + packageSize > Ids.Count())
                {
                    pack = Ids.Count() - counter;
                }

                result += BbProvider.UpdateModifiedOnById(Ids.Skip(counter).Take(pack), table, logger, countErrorTry);
                logger.Information($"{DateTime.Now} data - [{result}]");
                counter += packageSize;
            }

            return result;
        }

        private static int Delete(IEnumerable<Guid> Ids, UnwantedSql.DbProviderBase BbProvider, string table, Serilog.Core.Logger logger, int packageSize, int countErrorTry)
        {
            int result = 0;
            int counter = 0;
            int pack = packageSize;

            if (Ids == null) { return -1; }


            while (counter < Ids.Count())
            {
                if (counter + packageSize > Ids.Count())
                {
                    pack = Ids.Count() - counter;
                }

                result += BbProvider.DeleteById(Ids.Skip(counter).Take(pack), table, logger, countErrorTry);
                logger.Information($"{DateTime.Now} data - [{result}]");
                counter += packageSize;
            }

            return result;
        }
    }
}
