﻿using Microsoft.Extensions.Configuration;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System;

namespace Consimple.UpdateModifiedOn
{
    public class Config : ConfigBase
    {
        public string constMsConnectinString;
        public string constMyConnectinString;

        public int countErrorTry;
        public bool saveToFile;
        public int packageSize;

        public Config(string configFileName = "appConfig.json", string directoryPath = null)
        {
            directory = directoryPath ?? Directory.GetCurrentDirectory();
            Init(configFileName);
        }

        protected override void InitApp(IConfigurationRoot conf)
        {
            LogPath = conf.GetSection("settings")["loggerPath"] ?? directory;
            countErrorTry = conf.GetSection("settings")["countErrorTry"] == null? 3 : conf.GetSection("settings").GetValue<int>("countErrorTry");
            packageSize = Convert.ToInt32(conf.GetSection("settings")["maxSelectCount"]);
            saveToFile = conf.GetSection("settings").GetValue<bool>("saveToFile");
            constMsConnectinString = conf.GetSection("connectionStrings")["bpmConnection"];
            constMyConnectinString = conf.GetSection("connectionStrings")["perconaConnection"];
        }


        public List<Table> GetTableList(string configFileName, string _directory = null)
        {
            var conf = GetConf(configFileName, _directory);
            return GetTableListByConf(conf);
        }


        protected  List<Table> GetTableListByConf(IConfigurationRoot conf)
        {
            var sqlList = new List<Table>();
            var section = conf.GetSection("TablesList");
            var parseList = section.GetChildren();
            foreach (var item in parseList)
            {
                var sectionChild = new Table
                {
                    Queue = item.GetValue<string>("Queue"),
                    PerconaTable = item.GetValue<string>("perconaTable"),
                    BpmTable = item.GetValue<string>("bpmTable"),
                    ExtendedBpmColumnName = item.GetValue<string>("extendedBpmColumnName"),
                    ExtendedPerconaColumnName = item.GetValue<string>("extendedPerconaColumnName"),
                    Action = item.GetValue<Action>("action"),
                    SyncDirect = item.GetValue<SyncDirect>("syncDirect")
                };
                sqlList.Add(sectionChild);
            }
            return sqlList;
        }
    }


    public class Table
    {
        public string Queue { get; set; }
        public string PerconaTable { get; set; }
        public string BpmTable { get; set; }
        public string ExtendedBpmColumnName { get; set; }
        public string ExtendedPerconaColumnName { get; set; }
        public Action Action { get; set; }
        public SyncDirect SyncDirect { get; set; }
    }

    public enum SyncDirect
    {
        bpmToPercona,
        perconaToBpm,
        duplex
    }

    public enum Action
    {
        update,
        analysis,
        delete
    }
}
