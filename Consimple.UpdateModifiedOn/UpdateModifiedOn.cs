﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Consimple.UpdateModifiedOn
{
    public class UpdateModifiedOn
    {
        public Config config;
        private List<Table> TableList = new List<Table>();
        public List<IChannelPool> UpdChannelList;
        public UpdateModifiedOn()
        {
            config = new Config();
            TableList = config.GetTableList("TableList.json");
            UpdChannelList = BildChannelPool(TableList, config);
        }

        public IChannelPool GetUpdChannelByQueueName(string queueName)
        {
            return UpdChannelList.Find(x => x.Name == queueName);
        }
        public IChannelPool GetUpdChannelByQueueName(string queueName, out DateTime syncDate)
        {
            syncDate = DateTime.UtcNow;
            return UpdChannelList.Find(x => x.Name == queueName);
        }

        private List<IChannelPool> BildChannelPool(List<Table> TableList, Config config)
        {
            var res = new List<IChannelPool>();
            foreach (var table in TableList)
            {
                res.Add(new UpdChannelPool(table, config));
            }

            if (res.ChackNotDistinctName())
            {
                throw new Exception($"TableList mast contain only unic QueueName: [{string.Join(", ", res.GetNotDistinctName())}]!");
            }
            return res;
        }

    }
}
