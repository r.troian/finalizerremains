﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Threading;
using Consimple;
using System.Threading.Tasks;

namespace Consimple.FinaliserRemains.App
{
    class Program
    {
        public static string currentDirectoryPath => Directory.GetCurrentDirectory();
        public static Serilog.Core.Logger mainLog;
        static void Main(string[] args)
        {
            Config config = new Config();
            mainLog = Logger.InitConfiguration("A_Main", config.LogPath, true);
            var consoleLog = Logger.InitConfiguration("console",  config.LogPath,  true);
           

            FinaliserRemains finaliser;
            UnwantedSql.UnwantedSql unwantedSql;
            UpdateModifiedOn.UpdateModifiedOn updateModifiedOn;
            Workers.WorkerSync workerSync;

            mainLog.Information("Application starting");
            consoleLog.Information("Application starting");
            consoleLog.Information($"CurrentDirectoryPath: [{currentDirectoryPath}]\n");
      
            try
            {
                finaliser = new FinaliserRemains();
                workerSync = new Workers.WorkerSync();
                unwantedSql = new UnwantedSql.UnwantedSql();           
                updateModifiedOn = new UpdateModifiedOn.UpdateModifiedOn();
            }
            catch (Exception ex)
            {
                SysLogFatalError($"Init Error  Message:[{ex.Message}]\nStackTrace:[{ex.StackTrace}]", mainLog, consoleLog);
                Console.WriteLine($"\n\nPress any key to close this window. . .");
                Console.ReadKey();
                throw ex;
            }       

            try
            {
                mainLog.Information($"SequenceList.Count: [{finaliser.SequenceList.Count}]");
                mainLog.Information($"SqlChannelList.Count: [{unwantedSql.SqlChannelList.Count}]");
                mainLog.Information($"TableList.Count: [{updateModifiedOn.UpdChannelList.Count}]");
                mainLog.Information($"SyncList.Count: [{workerSync.WorkerChannelList.Count}]\n");
                
                int countSkip = 0;

                if(config.CountSkipStep > 0)
                {
                    mainLog.Information($"CountSkipStep: [{config.CountSkipStep}]\n");
                }

                var srt = ReadLineTimeout.ReadLine(15).Result;             
                int.TryParse(srt, out countSkip);

                if (srt == "NULL")
                {
                    countSkip = config.CountSkipStep;
                }

                DateTime syncDate = DateTime.UtcNow; 
                int countStep = 1; 

                foreach (var item in finaliser.SequenceList)
                {               
                    if(countStep < countSkip)
                    {
                        countStep++;
                        continue;
                    }

                    IChannelPool channel;

                    switch (item.QueueType)
                    {
                        case QueueType.WorkerSync: channel = workerSync.GetWorkerChannelByQueueName(item.QueueName, syncDate); break; 
                        case QueueType.UnwantedSql: channel = unwantedSql.GetSqlChannelByQueueName(item.QueueName); break;
                        case QueueType.UpdateModifiedOn:
                            {
                                channel = updateModifiedOn.GetUpdChannelByQueueName(item.QueueName, out syncDate);
                                Thread.Sleep(1000);
                            }
                            break;
                        default: throw new Exception($"Error undefined QueueType: [{item.QueueType}]");
                    }

                    consoleLog.Information($"Step : [{countStep++}/{finaliser.SequenceList.Count}] -[{item.Name}] - [{item.QueueName}]");

                    if (channel == null)
                    {
                        throw new Exception($"Not found Name: [{item.QueueName}] In [{item.QueueType}]");
                    }
                  
                    LogInfo(channel.Apply(), item.Name);

                    if (item.SleepTimeout > 0)
                    {
                        consoleLog.Information($"SleepTimeout: [{item.SleepTimeout/1000}] sec");
                        int val = item.SleepTimeout / 1000;
                        for(int it = 1; it <= val; it++)
                        {
                            Thread.Sleep(1000);
                            Console.Write($"\r{it}");
                        }
                        Console.Write($"\r");
                        Thread.Sleep(item.SleepTimeout - val * 1000);
                    }
                }


                //    Console.WriteLine($"SqlListCount = [{unwantedSql.SqlChannelList.Count}]\n");


                //    var sqlPool = unwantedSql.GetSqlChannelByQueueName("Test_Name_0");
                //    Console.WriteLine($"Test_Name_0 Apply: [{sqlPool.Apply()}]");

                //foreach (var item in unwantedSql.SqlChannelList)
                //{
                //    Console.WriteLine($"Directory = [{item._directory}]");
                //    Console.WriteLine($"Name = [{item.Name}]");
                //    Console.WriteLine($"DbType = [{item.DbType}]");
                //    Console.WriteLine($"SqlType = [{item.SqlType}]");
                //    Console.WriteLine($"SqlText = [{item.SqlText}]");
                //    Console.WriteLine($"CommandTimeout = [{item.CommandTimeout}]\n");
                //}


                //foreach (var item in updateModifiedOn.UpdChannelList)
                //{
                //    Console.WriteLine($"Name = [{item.Name}]");
                //    Console.WriteLine($"BpmTable= [{item.BpmTable}]");
                //    Console.WriteLine($"PerconaTable = [{item.PerconaTable}]");
                //    Console.WriteLine($"SyncDirect = [{item.SyncDirect}]");
                //    Console.WriteLine($"Action = [{item.Action}]");
                //    Console.WriteLine($"ExtendedBpmColumnName = [{item.ExtendedBpmColumnName}]");
                //    Console.WriteLine($"ExtendedPerconaColumnName = [{item.ExtendedPerconaColumnName}]");
                //}

                consoleLog.Information("-- Finish! --");
                mainLog.Information($"Application Finished!");

            }
            catch (Exception ex)
            {
                SysLogFatalError($"Work Error  Message:[{ex.Message}]\n StackTrace:[{ex.StackTrace}]", mainLog, consoleLog);
                Console.WriteLine($"\n\nPress any key to close this window. . .");
                Console.ReadKey();
                throw ex;
            }

            
            Console.WriteLine($"\n\nPress any key to close this window. . .");
            Console.ReadKey();
        }

        public static void SysLogFatalError(string errMessage, params Serilog.Core.Logger[] loggers)
        {
            foreach(var log in loggers)
            {
                log.Fatal(errMessage);
            }
        }

        public static void LogInfo(Dictionary<string, object> objs, string name)
        {
            mainLog.Information($"\"{name}\" : [");
            foreach (var obj in objs)
            {
                mainLog.Information($"\"{obj.Key}\" : \"{obj.Value}\"");
            }
            mainLog.Information("]");
        }
    }

    public class ReadLineTimeout
    {
        public static async Task<string> ReadLine(int delay)
        {
            var result = string.Empty;

            var read = Task.Run(() => { return Console.ReadLine(); });

            if (await Task.WhenAny(read, Task.Delay(delay * 1000)) == read)
            {
                return read.Result;
            }
            else
            {
                return "NULL";// string.Empty;
            }
        }
    }
}
